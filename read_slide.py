#!/bin/python

import os
import sys
import glob
import argparse
import traceback

import torch
import numpy as np
from torchvision import datasets, models, transforms

import matplotlib.pyplot as plt
from skimage.transform import resize

import pandas as pd
import random
import pickle
import math

from pathlib import Path

import pyvips as pv

from openslide import OpenSlide

sys.path.append('/mnt/slidl')
from slidl.slide import Slide
from slidl.analysis import Analysis
from slidl.processor import Processor
from slidl.models.tissuedetector import tissueDetector


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--wsi_dir', type=str, help='Whole slide images directory')
parser.add_argument('--output_dir', type=str, help='Tiles directory')
parser.add_argument('--run_name', type=str, help='Run name')
parser.add_argument('--tile_size', type=int, default=224, help="Tile size, def 224px")

args = parser.parse_args()
print(args)

wsi_paths=glob.glob(args.wsi_dir+'/**/*',recursive = True)
print(len(wsi_paths), ' slides found in ', args.wsi_dir)

results_path=args.output_dir + '_' + str(args.tile_size) + 'px'
if not os.path.isdir(results_path):
	os.makedirs(results_path)

run_name=args.run_name

level=0 # extract tiles at highest resolution
mult=1 # no scaling
tile_size=args.tile_size

libdict_file=results_path+'/'+run_name+'_lib_dict.pt'
print("Lib dictionary file: " + libdict_file)

# Initialize case tile dict, or load latest in-progress version to continue where we last left off
if os.path.exists(libdict_file):
	print('Loading in-progress lib dictionary from '+libdict_file)
	lib_dict = torch.load(libdict_file)
else:
	print("Creating new lib dictionary")
	lib_dict = {"slides": [], "size": tile_size, "mult": mult, "level": level}

print("Tile slides loop:")
i = 1
for slide_file in wsi_paths:
	print(str(i) + ': ' + slide_file)

	if slide_file in lib_dict['slides']:
		print("Already completed: "+slide_file)
	elif Path(slide_file).suffix not in ['.ndpi', '.tif', '.tiff']:
		print("WARNING: "+slide_file+"is not a stacked image file (NDPI, TIF), skipping")
		next
	elif slide_file not in lib_dict['slides']:
		print("Extracting tissue tiles from: "+slide_file)
		try:        
			# initialize Slide object from WSI path
			pathml_slide = Slide(slide_file, level=level, verbose=True).setTileProperties(tileSize=tile_size)
    
			# Perform deep tissue detection
			pathml_slide.detectTissue(modelStateDictPath='/mnt/slidl/slidl/models/deep-tissue-detector_densenet_state-dict.pt')
 
			# All methods will go into dictionary (Otsu, triangle, simple thresholding) 
			pathml_slide.detectForeground()

	    	# IF we had annotations we'd add them here.  Add annotations to Slide if it is a tumor WSI
			#if "tumor" in case:
			# note: we have the annotations in our repo because the original camelyon ones have lots of squiggly overlap;
		       	# these are the fixed ones so you don't have to. Feel free to use the originals by adding 
       			# acceptMultiPolygonAnnotations=True to the addAnnotations() call, though
       			# annotation_path = os.path.join(path_to_repo, 'lesion_annotations', case+'.xml')
				# Add annotations
				# pathml_slide.addAnnotations(annotation_path, negativeClass='negative')

			# Save pathml Slide to be reloaded later
			pathml_slide.save(folder=results_path)
 
			# Save in-progress directory
			print("Saving dictionary: "+libdict_file)

			# Add case and tiles to dict
			lib_dict["slides"].append(slide_file)
			torch.save(lib_dict, f=libdict_file)

		except BaseException as error:
			print("ERROR loading slide file " + slide_file) 
			traceback.print_exc()
			print()			
			pass

	i=i+1

# Save case/tile library
print("Saving completed dictionary"+libdict_file)
torch.save(lib_dict, f=libdict_file)

print('DONE: Total slides in dictionary:'+str(len(lib_dict["slides"])) )



