#!/bin/python

import os
import shutil
import sys
import glob
import argparse
import traceback
import warnings
import time
import csv

import torch
import numpy as np
from torchvision import datasets, models, transforms

import matplotlib.pyplot as plt
from skimage.transform import resize
#from sklearn.model_selection import train_test_split

import pandas as pd
import random
import pickle
import math

import pyvips as pv

from pathlib import Path

sys.path.append('/mnt/slidl')
from slidl.slide import Slide
from slidl.analysis import Analysis
from slidl.processor import Processor
from slidl.models.tissuedetector import tissueDetector

def parse_boolean(string):
        string = string.lower()
        if string in ["true", "yes", "1"]:
                return True
        return False

parser = argparse.ArgumentParser(description='Process some integers.')

parser.add_argument('--pml_dir', type=str, help='PML slide images directory')
parser.add_argument('--output_dir', type=str, help='Tiles directory')
parser.add_argument('--run_name', type=str, help='Run name')
parser.add_argument('--ann_dir', type=str, help='ASAP annotation files')
parser.add_argument('--max_tiles', default=200, type=int, help='')
parser.add_argument('--overlap', default=0.7, type=float, help='Max overlap threshold for annotations, def 0.7')
parser.add_argument('--annotations', default='IM', nargs='*',  help='Slide annotations to extract')
parser.add_argument('--negative', default='Neg', type=str, help="Negative annotation")
parser.add_argument('--extract_all', default='True', type=parse_boolean, help="Extract all PMLs (T) or only those with matchined XML (F)")

parser.add_argument('--tlt', type=float,  help='tissue level threshold, default: 0.995')
parser.add_argument('--fgt', type=float,  help='foreground threshold, default: 85')
#parser.add_argument('--tile_size', default=224, type=int, help='Tile size in px, default: 224')

args = parser.parse_args()
print(args)

if not os.path.exists(args.pml_dir) or not os.path.exists(args.ann_dir):
	print("Dir not found: ", args.pml_dir, " or ", args.ann_dir)
	quit()



max_tiles=args.max_tiles
run_name = args.run_name

output_dir = os.path.join(args.output_dir, run_name+'_'+str(args.max_tiles)+'_'+str(args.overlap))
if not os.path.isdir(output_dir):
	os.makedirs(output_dir)
if not os.path.isdir(os.path.join(output_dir, 'tile_dict')):
	os.makedirs(os.path.join(output_dir, 'tile_dict'))
print('Tile output to:', output_dir)

pml_paths=glob.glob(args.pml_dir+'/**/*.pml', recursive = True)
pml_paths.sort()
print(len(pml_paths), ' slides found in ', args.pml_dir)

xml_paths = glob.glob( os.path.join(args.ann_dir, '*.xml') )
xml_paths.sort()
print(len(xml_paths), ' annotations files found in ', args.ann_dir)

print('Slide annotation labels:', args.annotations)

# Copy annotated pmls to the processing directory
print("Copying pml files to ", os.path.join(output_dir, 'tile_dict'))

to_extract = []
if args.extract_all:
	print("Extracting tiles from all slides") 
	for pml_file in pml_paths:
		sample_name = Path(pml_file).stem
		to_extract.append(sample_name)
		if not os.path.exists( os.path.join(output_dir, 'tile_dict', sample_name + '.pml') ):
			shutil.copy2(pml_file, os.path.join(output_dir, 'tile_dict/') )
else:
	print("Extracting tiles only from annotated slides")
	for annotation_file in xml_paths:
			sample_name = Path(annotation_file).stem
			to_extract.append(sample_name)
			#print(sample_name)
			pml_file = [s for s in pml_paths if os.path.basename(sample_name) in s][0]
			if not os.path.exists( os.path.join(output_dir, 'tile_dict', sample_name + '.pml') ):
				shutil.copy2(pml_file, os.path.join(output_dir, 'tile_dict/') )


pml_glob = glob.glob(os.path.join(output_dir, 'tile_dict', '*.pml'))
print(len(pml_glob), ' pml files copied')
                      
tile_counts = {}
if os.path.exists(os.path.join(output_dir, 'tile_counts.p')):
    tile_counts = pickle.load(open(os.path.join(output_dir, 'tile_counts.p'), 'rb'))

libdict_file = os.path.join(output_dir, 'tile_dict', run_name+'_lib_dict.pt')
print("Lib dictionary file: " + libdict_file)

level=0 # extract tiles at highest resolution
mult=1 # no scaling
tile_size=224 

orig_lib_dict_file = glob.glob(os.path.join(args.pml_dir, '*_lib_dict.pt'))
if len(orig_lib_dict_file) > 0:
	orig_lib_dict = torch.load(orig_lib_dict_file[0])
	res = [ele for ele in orig_lib_dict.keys() if (ele in ['size', 'mult', 'level'])]
	if len(res) == 3:
		level = orig_lib_dict['level']
		mult = orig_lib_dict['mult']
		tile_size = orig_lib_dict['size']
	else:
		print('No info found for level, mult, or tile size in ' + orig_lib_dict_file[0] + ' using defaults.')

seed = 366
random.seed(seed)

# Initialize case tile dict, or load latest in-progress version to continue where we last left off
if os.path.exists(libdict_file):
    print('Loading in-progress lib dictionary from '+libdict_file)
    lib_dict = torch.load(libdict_file)
else:
    print("Creating new lib dictionary")
    lib_dict = {"slides": [], "size": tile_size, "mult": mult, "level": level, "tissueLevelThreshold": args.tlt, "foregroundLevelThreshold": args.fgt}

warnings.simplefilter('ignore')

global_channel_sums = np.zeros(3)
global_channel_squared_sums = np.zeros(3)
global_tile_count = 0
global_ann_info = {'slides': {}, 'overlap': args.overlap, 'annotations':args.annotations, 'tile_size': tile_size, "tissueLevelThreshold": args.tlt, "foregroundLevelThreshold": args.fgt}

for sample_name in to_extract:
	tlt = args.tlt # 0.995
	if tlt is None: tlt = False
	fgt = args.fgt # 85
	if fgt is None: fgt = False

	#if not sample_name == '13H 05569 PR1 BED 060 1 B2842 B': continue
	#if not sample_name == "PS16.14348 A1-1 L1-3": continue

	print('Sample:', sample_name)
	pml_file = [s for s in pml_glob if os.path.basename(sample_name) in s][0]
	ann_file = [s for s in xml_paths if os.path.basename(sample_name) in s]	
	ann_file = ann_file[0] if len(ann_file) > 0 else None
	print(ann_file)		

	if pml_file in lib_dict['slides']:
		print("Already extracted " + pml_file + " skipping.")
		global_channel_sums = np.add(global_channel_sums, tile_counts[sample_name]['channel_data']['channel_sums'])
		global_channel_squared_sums = np.add(global_channel_squared_sums, tile_counts[sample_name]['channel_data']['channel_squared_sums'])
		global_tile_count = global_tile_count + tile_counts[sample_name]['channel_data']['num_tiles']
		continue
	
	print('\tExtracting ' + pml_file )
	try:
		slide = Slide(pml_file, level=level)
	
		# Add annotations
		if ann_file is not None:
			print('\tAnnotations ' + ann_file)
			slide.addAnnotations(ann_file, classesToAdd=args.annotations, negativeClass=args.negative, overwriteExistingAnnotations=True)

		slide.save(folder=os.path.join(output_dir, 'tile_dict'))

		total_tiles = slide.getTileCount(tissueLevelThreshold = tlt, foregroundLevelThreshold = fgt)
		print(total_tiles, " total tiles at tissue threshold ", tlt, " and foreground threshold ", fgt)

		max_tiles = args.max_tiles
		main_ann = args.annotations[0]
		if slide.hasAnnotations(): 
			print("Extracting max ", max_tiles, " for main annotation:", main_ann)
			num_im = slide.extractAnnotationTiles("/tmp", returnOnlyNumTilesFromThisClass = main_ann, tissueLevelThreshold = tlt, foregroundLevelThreshold = fgt)
		
			print('\tTotal tiles for main annotation ', main_ann, ':', num_im)
			while tlt and num_im < max_tiles:
				#print('\t',main_ann, ' ',  num_im, ' TLT:',tlt, ' ', type(tlt))
				tlt = round(tlt - 0.05,3)
				if tlt <= 0.01: tlt = 0.05 
				num_im = slide.extractAnnotationTiles("/tmp", returnOnlyNumTilesFromThisClass = main_ann, tileAnnotationOverlapThreshold = args.overlap, tissueLevelThreshold = tlt, foregroundLevelThreshold = fgt)
				if tlt <= 0.05: break
			
			global_ann_info['slides'][sample_name] = {'tissueLevelThreshold': tlt, 'foregroundLevelThreshold': fgt, 'tiles_extracted': max_tiles}
			print("\tExtracting ", max_tiles, " total tiles per class")

			channel_data = slide.extractAnnotationTiles(output_dir, otherClassNames = args.annotations[1:], tileAnnotationOverlapThreshold = args.overlap, numTilesToExtractPerClass = max_tiles,  tissueLevelThreshold = tlt, foregroundLevelThreshold = fgt, extractSegmentationMasks = True)
		else: #TODO need to rethink this?
			print("\tExtracting unannotated tiles, unann class name:", args.annotations[1:])
			channel_data = slide.extractRandomUnannotatedTiles(output_dir, numTilesToExtract = max_tiles, unannotatedClassName = args.annotations[1], otherClassNames = args.annotations[0], extractSegmentationMasks = True, tissueLevelThreshold = tlt, foregroundLevelThreshold = fgt)

		# Tile counts for next steps
		tile_counts[sample_name] = {'max_per_class': max_tiles, 'channel_data': channel_data, 'total':total_tiles, 'tissuelevelthreshold': tlt, 'foregroundLevelThreshold': fgt}
		print("\tAll tile counts:", tile_counts[sample_name])
		print("******************")

		global_channel_sums = np.add(global_channel_sums, channel_data['channel_sums'])
		global_channel_squared_sums = np.add(global_channel_squared_sums, channel_data['channel_squared_sums'])
		global_tile_count = global_tile_count + channel_data['num_tiles']

		pickle.dump(tile_counts, open(os.path.join(output_dir, 'tile_counts.p'), 'wb'))

		# Add case and tiles to dict
		#print("Add to libdict and save:",libdict_file)
		lib_dict["slides"].append(pml_file)
		torch.save(lib_dict, f=libdict_file)
		sys.stdout.flush()
	except Exception as e:
		print(e.__class__, " error occurred: ",e)
		traceback.print_tb(e.__traceback__)
		traceback.print_exc()


total_pixels_per_channel = global_tile_count * tile_size * tile_size
global_channel_means = np.divide(global_channel_sums, total_pixels_per_channel)
global_channel_squared_means = np.divide(global_channel_squared_sums, total_pixels_per_channel)
global_channel_variances = np.subtract(global_channel_squared_means, np.square(global_channel_means))
global_channel_stds = np.sqrt(global_channel_variances * (total_pixels_per_channel / (total_pixels_per_channel-1)))
means_and_stds = {'channel_means': global_channel_means.tolist(), 'channel_stds': global_channel_stds.tolist()}

print(means_and_stds)
pickle.dump(means_and_stds, open(os.path.join(output_dir, 'trainval_channel_means_and_stds.p'), 'wb'))
pickle.dump(global_ann_info, open(os.path.join(output_dir, 'annotation_info.p'), 'wb'))

torch.save(lib_dict, f=libdict_file)
pickle.dump(tile_counts, open(os.path.join(output_dir, 'tile_counts.p'), 'wb'))
print('Slides annotated and extracted:',len(lib_dict))
print("*** Finished ***")

