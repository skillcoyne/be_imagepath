#!/bin/python

import os
import sys
import glob
import argparse
import traceback
import warnings
import time
from datetime import date
import csv
import copy
import shutil
import re

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import torchvision

from torchvision import datasets, models, transforms

import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import resize
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, precision_score, recall_score, roc_auc_score, roc_curve

import pandas as pd
import pickle

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import cm
from matplotlib.ticker import ScalarFormatter, NullFormatter
from skimage.transform import resize

import pyvips as pv

sys.path.append('/mnt/slidl')
from slidl.slide import Slide
from slidl.analysis import Analysis
from slidl.processor import Processor
from slidl.models.tissuedetector import tissueDetector


def parse_boolean(string):
	string = string.lower()
	if string in ["true", "yes", "1"]:
		return True
	return False

def parse_escaped_string(string):
	return string.replace("_", " ") 


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--results_dir', type=str, required=True, help='Trained model, output by train_tissue.py')
parser.add_argument('--pred_label', type=str, required=True, help='Label for prediction, must have been trained on and annotated prior')

parser.add_argument('--pml_dir', type=str, required=False, help='Annotated PML directory')
parser.add_argument('--labels', default='IM SQBS', nargs='*',  help='Slide labels default: IM SQBS')
parser.add_argument('--overlap', default=0.3, type=float, help='Max overlap threshold for annotations, def 0.3')

## Here for convinience but will be overwritten where possible
parser.add_argument('--tlt', type=float,  help='tissue level threshold, Defaults to info in the pml lib_dict file')
parser.add_argument('--fgt', default=85, type=float,  help='foreground threshold, default: 85. This will be overwritten by info in the pml lib_dict file')
parser.add_argument('--tile_size', default=224, type=int, help='Tile size in px, default: 224.  This will be overwritten by info in the pml lib_dict file')

parser.add_argument('--case', type=parse_escaped_string, help='Look at only the matching case')

args = parser.parse_args()
args.tlt = None

print(args)


def findBestTissueThreshold(slide, foregroundLevelThreshold=False, annotationOverlap=0.3, tileClass=False):
    annotated_tiles = 0
    tissueLevel = []
    # artifactLevel = []

    if not tileClass:
        raise ValueError("An annotated tileClass is required.")

    annotationClass = tileClass + 'Overlap'
    for tileAddress in slide.suitableTileAddresses(foregroundLevelThreshold=foregroundLevelThreshold):
        tileEntry = slide.tileDictionary[tileAddress]

        if annotationClass in tileEntry:
            if tileEntry[annotationClass] > annotationOverlap:
                annotated_tiles = annotated_tiles + 1
            if tileEntry[annotationClass] > 0:
                tissueLevel.append(tileEntry['tissueLevel'])
                # artifactLevel.append(tileEntry['artifactLevel'])

    return {'annotatedTilesCount': annotated_tiles, 'tissueLevelForAnnotatedClass': tissueLevel,
            'overlap': annotationOverlap, 'tileClass': tileClass, 'foregroundThreshold': foregroundLevelThreshold, 'minThreshold': np.quantile(tissueLevel, 0.2).item()}


params_file = glob.glob(os.path.join(args.results_dir, 'training_params.p'))
if len(params_file) <= 0:
    params_file = glob.glob(os.path.join(args.results_dir, 'training_params.csv'))

params_file = params_file[0]

pml_dir = None
if args.pml_dir is not None and os.path.exists(args.pml_dir):
    pml_dir = args.pml_dir 

if os.path.exists(params_file):
    if re.search('\\.csv$', os.path.basename(params_file)):
        params = pd.read_csv(params_file)
        labels = re.sub(r"\[|\]|'| ", "", params.loc[0,'labels']).split(",")
        tile_size = params.loc[0,'tile_size'].item()
        ann_params = pickle.load(open(os.path.join(params.loc[0,'tiles'], 'annotation_info.p'),'rb'))
        if pml_dir is None:
            pml_dir = os.path.join(params.loc[0,'tiles'], 'tile_dict')
    else:
        params = pickle.load(open(params_file, 'rb'))
        labels = params['labels']
        tile_size = params['tile_size'][0]
        if os.path.exists(os.path.join(params['tiles'][0], 'annotation_info.p')):
            ann_params = pickle.load(open(os.path.join(params['tiles'][0], 'annotation_info.p'),'rb'))
            fgt = False if ann_params['foregroundLevelThreshold'] is None else ann_params['foregroundLevelThreshold']
            overlap = ann_params['overlap']
        if pml_dir is None:
            pml_dir = os.path.join(params['tiles'][0], 'tile_dict')

    if args.pred_label not in labels:
        print(args.pred_label + " was not in the set of labels model was trained for: " + ", ".join(labels))
        quit()    
     
    #if args.tlt is not None:
    #    tlt = args.tlt
    #else:
    #    tlt = False if ann_params['tissueLevelThreshold'] is None else ann_params['tissueLevelThreshold']
else:
    print("Warning: no training_params.csv file available. Please be sure your annotation, training and inference parameters match. There are no checks.")
    tlt = args.tlt
    fgt = args.fgt
    tile_size = args.tile_size
    labels = args.labels
    overlap = args.overlap
    pml_dir = args.pml_dir
    if pml_dir is None or tlt is None or fgt is None or tile_size is None or labels is None or overlap is None:
        parser.print_help()
        quit()

print("PML dir:", pml_dir)
print("Tile size:", tile_size)

today = date.today()

output_dir = os.path.join(args.results_dir, 'classification_results')
    
if not args.case is None:
    output_dir = os.path.join(args.results_dir, args.case)

if args.tlt is not None:
    output_dir = output_dir + "_" + str(args.tlt)


if os.path.exists(output_dir):
    shutil.rmtree(output_dir)

os.makedirs(output_dir)
os.makedirs(os.path.join(output_dir, 'pml'))	


cohorts = pickle.load(open(glob.glob(os.path.join(args.results_dir, 'classification_cohorts_*'))[0], 'rb'))
modelPath = glob.glob(os.path.join(args.results_dir, 'classification_best_model*'))[0]

means_and_stds = cohorts['train_val_norm']
cases = { key: cohorts['cases'][key] for key in ['train','test','val'] }

lib_dict_file = glob.glob(os.path.join(pml_dir, '*_lib_dict.pt'))
if len(lib_dict_file) > 0:
    lib_dict = torch.load(lib_dict_file[0])

tile_counts = pd.DataFrame(index=cases['val']+cases['test'],data={'tiles_above_0.5':0, 'total tiles':0})

pml_cases = glob.glob(os.path.join(pml_dir, '*.pml'))
print(len(pml_cases))

dataTransforms = transforms.Compose([
    transforms.Resize(tile_size),
    transforms.ToTensor(),
    transforms.Normalize(means_and_stds['channel_means'], means_and_stds['channel_stds'])])

# TODO if the training script is changed to allow different models this will need rechecking
trainedModel = models.vgg19_bn(pretrained=False)
num_ftrs = trainedModel.classifier[6].in_features
trainedModel.classifier[6] = nn.Linear(num_ftrs, len(labels))
trainedModel.load_state_dict(torch.load(modelPath))

probability_thresholds = np.append(np.round(np.arange(0, 1, 0.005),3),[0.9925, 0.995, 0.9975, 0.999, 0.99925, 0.9995, 0.99975, 0.9999, 0.999925, 0.99995, 0.999975, 0.99999, 0.9999925, 0.999995, 0.9999975, 0.999999]).tolist()
threshold_accuracies_all_slides = []

os.makedirs(os.path.join(output_dir, 'thresholds'))

## Labels must be in alphabetical order and slidl does not check this
labels.sort()
print('Labels:', labels)
#i = 0
primary_label = args.pred_label
print("Assessing classification for ", primary_label)
for case in cases['val'] + cases['test']:
    case_tlt = False # tlt
    fgt = 85 
    if not args.case is None and not args.case == os.path.basename(case):
        continue
#    if i > 10: break

    print('Case: ', case)
    pml_file = [s for s in pml_cases if os.path.basename(case) in s][0]
    print('PML: ', pml_file)
    
    try:
        slidl_slide = Slide(pml_file, verbose=True)

        #btt = findBestTissueThreshold(slidl_slide, fgt, overlap, primary_label)
        #pickle.dump(btt, open(os.path.join(output_dir, 'thresholds', slidl_slide.slideFileName+'.p'), 'wb'))
	
        #if args.tlt is None:
        #    case_tlt = btt['minThreshold']

        if not slidl_slide.hasAnnotations():
            raise Exception('Annotations are required for analysis of classifier but no annotations found in ' + pml_file)

        # This will infer all tiles that meet the threshold stated. Leaving tissue level thresholds out means subsequent analysis has to be aware of the tiles tissue
        #slidl_slide.inferClassifier(trainedModel, classNames=args.labels, dataTransforms=dataTransforms, foregroundLevelThreshold=fgt)
        print("Inferring classifier with tissue threshold=" + str(case_tlt) + " and foreground threshold=" + str(fgt))
        slidl_slide.inferClassifier(trainedModel, classNames=labels, dataTransforms=dataTransforms, tissueLevelThreshold=case_tlt, foregroundLevelThreshold=fgt)
        slidl_slide.inferenceThresholds = {'tissueLevelThreshold':case_tlt, 'foregroundLevelThreshold':fgt, 'className':labels, 'pred_class':args.pred_label}

        if case in cases['val']:
            threshold_accuracies = slidl_slide.classifierMetricAtThreshold(primary_label, probability_thresholds, tileAnnotationOverlapThreshold=overlap, metric='accuracy', assignZeroToTilesWithoutAnnotationOverlap=True)
            threshold_accuracies_all_slides.append(threshold_accuracies)
        
        #print('Tiles>0.5 for IM:', slidl_slide.numTilesAboveClassPredictionThreshold('IM', 0.5), ' total tiles:', slidl_slide.getTileCount(foregroundLevelThreshold=fgt, tissueLevelThreshold=tlt))

        tile_counts.at[os.path.basename(case),'tiles_above_0.5'] = slidl_slide.numTilesAboveClassPredictionThreshold(primary_label, 0.5)
        tile_counts.at[os.path.basename(case),'total tiles'] = slidl_slide.getTileCount(foregroundLevelThreshold=fgt, tissueLevelThreshold=case_tlt)

        slidl_slide.visualizeClassifierInference(primary_label, folder=os.path.join(output_dir, primary_label))
        print("Writing pml to ", output_dir)
        slidl_slide.save(folder=os.path.join(output_dir, 'pml'))
        plt.close('all')
    except Exception as e:
        print(e.__class__, " error occurred")
        print(e.__traceback__)
	#traceback.print_exception(e)
        traceback.print_exc()


if not args.case is None:
    print(args.case + " completed")
    quit()

print("Writing threshold accuracy to ", output_dir)
pickle.dump(threshold_accuracies_all_slides, open(os.path.join(output_dir, 'threshold_accuracy_all_slides.p'), 'wb'))

threshold_avg_accuracies = np.mean(np.array(threshold_accuracies_all_slides), axis=0)
index_of_best_classification_threshold = np.argmax(threshold_avg_accuracies)
best_classification_threshold = probability_thresholds[index_of_best_classification_threshold]
threshold_at_05 = threshold_avg_accuracies[probability_thresholds.index(0.5)]

print("Best tile-level validation accuracy:", threshold_avg_accuracies[index_of_best_classification_threshold])
print("Threshold that gives the best tile-level validation accuracy:", best_classification_threshold)
print("Threshold at probability of 0.5:", threshold_at_05)

thresholds = pd.DataFrame(data={'threshold_avg_accuracies':threshold_avg_accuracies})
thresholds.to_csv(os.path.join(output_dir, 'thresholds.csv'))

plt.figure(figsize=(7.5,6))
plt.plot(probability_thresholds, threshold_avg_accuracies)
plt.xlim(-0.05, 1.05)
plt.ylim(0, 1.0)
plt.hlines(y=np.median(np.array(threshold_avg_accuracies)), xmin=0, xmax=1, linestyles='dashed', colors='grey', alpha=0.5)
plt.scatter(x=0.5, y=threshold_at_05 , color='red')
plt.text(x=0.45, y=np.round(threshold_at_05+0.02, 2), s='P(0.5)='+str(np.round(threshold_at_05,2)), ha='center')
plt.title("Tile-level accuracy on val vs. tile probability threshold for " + primary_label)
plt.xlabel('Probability threshold for determination\nof number of tiles showing ' + primary_label)
plt.ylabel("Tile accuracy for detection with\nthresholded number of tiles")
plt.savefig(os.path.join(output_dir, 'classification_validation_tile_accuracy_vs_probability_threshold.png'))
plt.show(block=False)
plt.close()


tile_counts.to_csv(os.path.join(output_dir, 'tile_counts.csv'), index_label = 'Case')


print('Finished')

