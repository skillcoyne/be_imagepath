#!/bin/python

import os
import sys
import glob
import argparse
import traceback
import warnings
import time
import datetime
from datetime import date
import csv
import shutil
from re import match

from tqdm import tqdm

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import torchvision

from torchvision import datasets, models, transforms

import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import resize
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, precision_score, recall_score, roc_auc_score, roc_curve

from pathlib import Path
import pandas as pd
import random
import pickle
import math

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import cm
from matplotlib.ticker import ScalarFormatter, NullFormatter
from skimage.transform import resize

import pyvips as pv

sys.path.append('/mnt/slidl')
from slidl.slide import Slide
from slidl.analysis import Analysis
from slidl.processor import Processor
from slidl.models.tissuedetector import tissueDetector

from classification_function import train_classification_model, visualize_batch

print("PyTorch version:", torch.__version__)
print("pyvips version:", pv.__version__)

def parse_boolean(string):
	string = string.lower()
	if string in ["true", "yes", "1"]:
		return True
	return False


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--tiles_dir', type=str, required=True, help='Tiled slide images directory, output by extract_tiles.py')
parser.add_argument('--output_dir', type=str, required=True, help='')
parser.add_argument('--slide_list',type=str,help='csv file listing slides with `Cohort` (train/test/val) column and a column to be used for labels (see `label` option)')
parser.add_argument('--label', default='Pathology', type=str,  help='Column to get slide labels from in csv file')
parser.add_argument('--run_name', type=str, required=False, default=None, help='Name this training run')
parser.add_argument('--epochs', type=int, default=30, help='Number of epochs to train')
parser.add_argument('--batchsize', default=48, type=int, help='Batch size')
parser.add_argument('--pretrain', default='True', type=parse_boolean, help='Flag for using pretrained network (vgg19 currently)')

args = parser.parse_args()
print(args)

# Set up output directory
today = date.today()

if not args.run_name: args.run_name = "run" 

args.run_name = args.run_name + '_' + today.strftime("%Y%m%d") + "_epochs_" + str(args.epochs) + '_vgg19pretrain_' + str(args.pretrain) + '_batch' + str(args.batchsize)

results_path = os.path.join(args.output_dir, args.run_name)
if os.path.exists(results_path):
    shutil.rmtree(results_path)

os.makedirs(results_path)
print("Results being written to: "+ results_path)

tile_size = 224
lib_dict_file = glob.glob(os.path.join(args.tiles_dir, '*_lib_dict.pt'))
print(lib_dict_file)
#if len(lib_dict_file) > 0:
#	lib_dict = torch.load(lib_dict_file[0])
#	res = [ele for ele in lib_dict_file.keys() if (ele in ['size'])]
#	if 'size' in lib_dict.keys(): tile_size = lib_dict['size']
#	if len(res) < 1:
#		print('Missing info for tile size in ' + lib_dict_file[0] + ' using default (' + str(tile_size) + ').')

params = {'pytorch_version':torch.__version__, 'tiles':[args.tiles_dir], 'lib_dict':[lib_dict_file], 'batchsize':[args.batchsize], 'pretrain':[args.pretrain], 'epochs':[args.epochs], 'tile_size':[tile_size]}

seed = 366
random.seed(seed)

params['seed'] = [seed]

warnings.simplefilter('ignore')

print(args.tiles_dir)
# Reload mean and std data for normalization
if len(glob.glob(os.path.join(args.tiles_dir, '*channel_means_and_stds.p'))) <= 0:
	print("Missing channel_means_and_stds file in the " + args.tiles_dir + " directory")
	quit()
cache_file = glob.glob(os.path.join(args.tiles_dir, '*channel_means_and_stds.p'))[0]
per_case_file = os.path.join(args.tiles_dir, 'tile_counts.p')
if os.path.exists(per_case_file):
	print("Load pre-cached tile info:",per_case_file)
	per_case = pickle.load(open(per_case_file, 'rb'))
#	means_and_stds = pickle.load(open(cache_file, 'rb'))
else:
	print("channel means not available, run extract_tiles.py script first")
	quit()

cases = list(per_case.keys())
cases = list(map(lambda x: os.path.join(args.tiles_dir, 'tiles', x), cases))
print('Cases:', len(cases))


df = pd.read_csv(args.slide_list, sep=',', header=0)


if args.label not in list(df.columns):
	print(args.label + ' not an available column in the provided csv: ' + args.slide_list)
	quit()

labels = list(pd.unique(df[args.label]))

params['labels'] = labels

print(df)

# cross val splits from file
folds = list(filter(lambda v: match('^CrossVal_\\d+', v), df.columns))
foldperf={}
for fold in folds:
	print("Fold {}".format(folds.index(fold)+1))
	train_cases = list(map(lambda sample: os.path.join(args.tiles_dir, 'tiles', sample), df.loc[df[fold] == "Train",'Basename'].tolist()))
	val_cases = list(map(lambda sample: os.path.join(args.tiles_dir, 'tiles', sample), df.loc[df[fold] == "Val",'Basename'].tolist()))

	#params['splits'] = [args.slide_list]

	#print('Splits:', df[args.cohort].value_counts())
	#print(params)

	# make sure only the cases being included in train/val sets are used for normalization
	global_channel_sums = np.zeros(3)
	global_channel_squared_sums = np.zeros(3)
	global_tile_count = 0

	# Make sure we're only using the normalization from train/val cases
	for case in (list(map(lambda case: os.path.basename(case), train_cases + val_cases))):
		if case not in per_case:
			continue
		channel_data = per_case[case]['channel_data']
		global_channel_sums = np.add(global_channel_sums, channel_data['channel_sums'])
		global_channel_squared_sums = np.add(global_channel_squared_sums, channel_data['channel_squared_sums'])
		global_tile_count = global_tile_count + channel_data['num_tiles']

	total_pixels_per_channel = global_tile_count * tile_size * tile_size
	global_channel_means = np.divide(global_channel_sums, total_pixels_per_channel)
	global_channel_squared_means = np.divide(global_channel_squared_sums, total_pixels_per_channel)
	global_channel_variances = np.subtract(global_channel_squared_means, np.square(global_channel_means))
	global_channel_stds = np.sqrt(global_channel_variances * (total_pixels_per_channel / (total_pixels_per_channel-1)))

	print('Channel means:', global_channel_means)
	print('Channel stds:', global_channel_stds)


	per_fold_means_and_stds = { fold: {'channel_means': global_channel_means.tolist(), 'channel_stds': global_channel_stds.tolist()}}
	means_and_stds = { 'channel_means': global_channel_means.tolist(), 'channel_stds': global_channel_stds.tolist() }
	print(means_and_stds)
	pickle.dump(per_fold_means_and_stds, open(os.path.join(results_path, 'trainval_channel_means_and_stds.p'), 'wb'))

	cases_file = os.path.join(results_path, 'classification_cohorts_' + fold + '_' + today.strftime("%Y%m%d") +'.p')
	pickle.dump({'cases': {'train':train_cases,  'val':val_cases}, 'train_val_norm': {'channel_means':global_channel_means, 'channel_stds':global_channel_stds}},
	open(cases_file, 'wb'))

	class_names = labels
	class_names.sort() # class names MUST be sorted alphabetically
	print('Class names: ', class_names)

	# Choose minibatch size according to your GPU.
	batch_size = args.batchsize
	# Select number of epochs
	num_epochs = args.epochs

	# Define training and validation augmentations
	# We are introducing strong HSV color jittering to account for the different dates and labs where slides were stained
	data_transforms = {
		'train': transforms.Compose([
			transforms.Resize(tile_size),
			transforms.RandomVerticalFlip(),
			transforms.RandomHorizontalFlip(),
			transforms.ColorJitter(brightness=0, contrast=0, saturation=1, hue=.5), # can be added and shown for example
			transforms.ToTensor(),
			transforms.Normalize(means_and_stds['channel_means'], means_and_stds['channel_stds']) # can be added and shown for example
		]),
		'val': transforms.Compose([
			transforms.Resize(tile_size),
			transforms.ToTensor(),
			transforms.Normalize(means_and_stds['channel_means'], means_and_stds['channel_stds'])
			]),
	}
	sys.stdout.flush()

	print("PyTorch datasets from ", args.tiles_dir)

	# Create train and val PyTorch datasets
	train_dataset = torch.utils.data.ConcatDataset([datasets.ImageFolder(train_case, data_transforms['train']) for train_case in train_cases])
	val_dataset = torch.utils.data.ConcatDataset([datasets.ImageFolder(val_case, data_transforms['val']) for val_case in val_cases])

	targets = []
	for _, target in train_dataset:
		targets.append(target)
	targets = torch.tensor(targets)

	# Compute samples weight (each sample should get its own weight) for balanced sampling
	class_sample_count = torch.tensor( [(targets == t).sum() for t in torch.unique(targets, sorted=True)] )
	weight = 1.0 / class_sample_count.float()
	samples_weight = torch.tensor([weight[t] for t in targets])

	image_datasets = {'train': train_dataset, 'val': val_dataset}
	dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'val']}
	print('Dataset sizes:', dataset_sizes)
    
	dataloaders = {}
	# weighted sampler selects samples based on the weights provided to balance the training dataset
	sampler = torch.utils.data.WeightedRandomSampler(weights=samples_weight, num_samples=len(samples_weight), replacement=True)
	dataloaders['train'] = torch.utils.data.DataLoader(image_datasets['train'], batch_size=batch_size, drop_last=True, num_workers=16, sampler=sampler)
	dataloaders['val'] = torch.utils.data.DataLoader(image_datasets['val'], batch_size=batch_size, num_workers=16, shuffle=True)

	inputs, classes = next(iter(dataloaders['train']))
	out = torchvision.utils.make_grid(inputs)
	visualize_batch(out, mean=means_and_stds['channel_means'], std=means_and_stds['channel_stds'], title=os.path.join(results_path, "Fold_"+str(folds.index(fold)+1)+"_visualize_augmentation"))
 
	# Check for GPU
	#device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
	if torch.cuda.is_available():
		print("Training on GPU")
	else:
		print("Training on CPU")
    
	# Set up pretraining (adjustable! show how training from scratch takes much more training time)
	# TODO: May need to look at other networks (ResNet etc)
	print('Classes:', class_names)
	model_ft = models.vgg19_bn(pretrained=args.pretrain)
	num_ftrs = model_ft.classifier[6].in_features
	model_ft.classifier[6] = nn.Linear(num_ftrs, len(class_names))

	params['model'] = ['vgg19_bn']

	# Select loss function (adjustable!)
	criterion = nn.CrossEntropyLoss()
	params['loss_func'] = [type(criterion).__name__]

	# Select optimization function (adjustable! try Adam as well)
	optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)
	#optimizer_ft = optim.Adam(model_ft.parameters(), lr=0.001)
	params['opt_func'] = [type(optimizer_ft).__name__]

	# Set up learning rate decay (very very adjustable)
	# Decay LR by a factor of 0.1 every 7 epochs
	exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)
	params['decay_step'] = [7]
	params['decay_gamma'] = [0.1]

	print(params)
	#pd.DataFrame.from_dict(params).to_csv(os.path.join(results_path, 'training_params.csv'), index=False)

	pickle.dump(params, open(os.path.join(results_path, 'training_params.p'),'wb'))

	start_time = time.time()
	# run training

	model_ft, learningStats = train_classification_model(model_ft, dataloaders, dataset_sizes, criterion, optimizer_ft, exp_lr_scheduler, num_epochs=num_epochs, classes=class_names,
													 earlystop=0.1)

	learning_stats_file = os.path.join(results_path, 'classification_learning_stats_' + today.strftime("%Y%m%d") +'.p')
	pickle.dump(learningStats, open(learning_stats_file, 'wb'))

	class_model_file = os.path.join(results_path, 'classification_best_model_' + today.strftime("%Y%m%d") + '_ft.pt')
	torch.save(model_ft, class_model_file)

	time_elapsed = time.time() - start_time
	print('Fold ', folds.index(fold)+1, ' complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

	foldperf['fold{}'.format(folds.index(fold)+1)] = learningStats

print('Complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
print("**** Finished, outputs in: ", results_path, " ****")


