#!/bin/bash

#! Give your job a name
#SBATCH -J train_test_network
#! How many cores per task?
#SBATCH --cpus-per-task=4
#! How much memory do you need?
#SBATCH --mem=16G
#! How much wallclock time will be required?
#SBATCH --time=24:00:00
#! What types of email messages do you wish to receive?
#SBATCH --mail-type=END,FAIL
#! Specify your email address here otherwise you won't recieve emails!
#SBATCH --mail-user=sk875@cam.ac.uk
#! Uncomment this to prevent the job from being requeued (e.g. if
#! interrupted by node failure or system downtime):
##SBATCH --no-requeue
#! Make sure to use the gpunodes partition
#SBATCH -p gpunodes
#! Specify a GRES (generic resource) of type gpu and how many you want
#SBATCH --gres gpu:1
#SBATCH --output=logs/train_tissue_slurm-%j.out


SCRATCH='/scratchb/stlab-icgc/users/killco01'
SCRIPT='be_imagepath/train_simple.py'
#SING='slide_proc_pytorch1.8.simg'
SING='slide_proc.simg'

usage() {                                 # Function: Print a help message.
  echo "Usage: $0 [ -t tiles dir REQ ] [ -o output dir REQ ] [ -r run name REQ ] [ -l labels (IM SQBS) REQ]  
	OPTIONAL: [ -d datasize (0-1.0 def 1.0) ] [ -e epochs (def 30) ] [ -p pretrain (def True) ] [ -b batchsize (def 48) ] [ -s tsv file with train/val/test splits ]" 1>&2 
}
exit_abnormal() {                         # Function: Exit with error.
  usage
  exit 1
}

while getopts "t:o:d:e:r:p:b:s:l:" options; do
	case "${options}" in
		t) 
			tiles=${OPTARG}
			;;
		o)
			outdir=${OPTARG}
			;;
		d)
			datasize="--datasize ${OPTARG}"
			;;
		e)
			epochs="--epochs ${OPTARG}"
			;;
		r)
			runname="--run_name ${OPTARG}"
			;;
		p)
			pretrain="--pretrain ${OPTARG}"
			;;
		b)
			batch="--batchsize ${OPTARG}"
			;;
		s)
			splits="--splits /mnt/${OPTARG}"
			;;
		l)
			labels=$(echo $OPTARG | tr "," "\n")
			labels="--labels ${labels}"
			;;
 		*)                                    # If unknown (any other) option:
                        exit_abnormal                       # Exit abnormally.
                        ;;

		
	esac
done

if [ "$tiles" == "" ] || [ "$outdir" == "" ] || [ "$runname" == "" ]; then
        exit_abnormal
fi



cat << OUT
Tiles ${tiles} \
Outdir ${outdir} \
Run name ${runname} \
Datasize ${datasize} \
Epochs ${epochs} \
Labels ${labels} \
Pretrain: ${pretrain} \
Batch: ${batch} \
Splits: ${splits} \
Overlap: ${overlap}
OUT
exit


if [ "$tiles" == "" ] || [ "$outdir" == "" ] || [ "$runname" == "" ] || [ "$labels" == "" ]; then
	echo "Missing required input for tiles, output director, labels, or run name"
	exit_abnormal
fi




# --nv mounts Nvidia CUDA libraries
date
singularity exec \
  --nv \
  --bind ${SCRATCH}/singularity_recipe:/mnt ${SING} \
  python3 /mnt/${SCRIPT} \
  --tiles_dir /mnt/${tiles} \
  --output_dir /mnt/${outdir} ${epochs} ${runname} ${labels} ${pretrain} ${batch} ${splits}
date
