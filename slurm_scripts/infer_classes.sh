#!/bin/bash

#! Give your job a name
#SBATCH -J infer_classes
#! How many cores per task?
#SBATCH --cpus-per-task=4
#! How much memory do you need?
#SBATCH --mem=30G
#! How much wallclock time will be required?
#SBATCH --time=36:00:00
#! What types of email messages do you wish to receive?
#SBATCH --mail-type=END,FAIL
#! Specify your email address here otherwise you won't recieve emails!
#SBATCH --mail-user=sk875@cam.ac.uk
#! Uncomment this to prevent the job from being requeued (e.g. if
#! interrupted by node failure or system downtime):
##SBATCH --no-requeue
#! Make sure to use the gpunodes partition
#SBATCH -p gpunodes
#! Specify a GRES (generic resource) of type gpu and how many you want
#SBATCH --gres gpu:1
#SBATCH --output=logs/infer_classes-%j.out


SCRATCH='/scratchb/stlab-icgc/users/killco01'
SCRIPT='be_imagepath/infer_class.py'
#SING='slide_proc_pytorch1.8.simg'
SING='slide_proc.simg'

usage() {                                 # Function: Print a help message.
  echo "Usage: $0 [ -m model dir (trained model) REQ ] [ -p pml directory for inference (not annotated) ] [-l labels (IM,SQBS) ] [ -t tlt (DEF 0.65) ] [ -b prob (DEF 0.95) ]" 1>&2  
}
exit_abnormal() {                         # Function: Exit with error.
  usage
  exit 1
}

while getopts ":p:m:b:l:t:" options; do
        case "${options}" in
                m)
                        modeldir=${OPTARG}
			modeldir="--model_dir /mnt/${modeldir}"
                        ;;
		p)
			pmldir=${OPTARG}
			pmldir="--pml_dir /mnt/${pmldir}"
			;;
		
		l)
                        labels=$(echo $OPTARG | tr "," "\n")
                        labels="--labels ${labels}"
                        ;;
		t)
			tlt="--tlt ${OPTARG}"
			;;

		b)
			prob="--prob ${OPTARG}"
			;;
        esac
done

if [ "$modeldir" == "" ]; then
        exit_abnormal
fi

# --nv mounts Nvidia CUDA libraries
date
singularity exec \
  --nv \
  --bind ${SCRATCH}/singularity_recipe:/mnt ${SING} \
  python3 /mnt/${SCRIPT} \
  $modeldir $pmldir $labels $prob
date
