#!/bin/bash

#! Give your job a name
#SBATCH -J BE_slide_tile
#! How many cores per task?
#SBATCH --cpus-per-task=4
#! How much memory do you need?
#SBATCH --mem=20G
#! How much wallclock time will be required?
#SBATCH --time=72:00:00
#! What types of email messages do you wish to receive?
#SBATCH --mail-type=BEGIN,END,FAIL
#! Specify your email address here otherwise you won't recieve emails!
#SBATCH --mail-user=sk875@cam.ac.uk
#SBATCH -p gpunodes
#! Specify a GRES (generic resource) of type gpu and how many you want
#SBATCH --gres gpu:1

#SBATCH --output=logs/slide_tile-GPU_slurm-%j.out


SCRATCH='/scratchb/stlab-icgc/users/killco01'
SCRIPT='be_imagepath/read_slide.py'


usage() {                                 # Function: Print a help message.
  echo "Usage: $0 [ -w wsi dir REQ ] [ -o output dir REQ ] [ -r run name REQ ] [ -s  tile size (in px) REQ]" 1>&2
}
exit_abnormal() {                         # Function: Exit with error.
  usage
  exit 1
}

while getopts "w:o:r:s:" options; do
	echo "${options} ${OPTARG}"
        case "${options}" in
                w)
                        wsi=${OPTARG}
                        ;;
                o)
			outdir=${OPTARG}
			;;
		r)
			runname=${OPTARG}
			;;
		s)
			tile_size=${OPTARG}
			;;

   esac
done

if [ "$wsi" == "" ] || [ "$outdir" == "" ] || [ "$runname" == "" ] || [ "$tile_size" == "" ]; then
        exit_abnormal
fi


runname="${runname}_${tile_size}px"

date
singularity exec \
  --nv \
  --bind $SCRATCH/singularity_recipe:/mnt slide_proc.simg \
  python /mnt/$SCRIPT \
  --wsi_dir /mnt/$wsi \
  --output_dir /mnt/$outdir \
  --run_name $runname \
  --tile_size $tile_size
date
