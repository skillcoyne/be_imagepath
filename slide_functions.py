import sys
import random
import shutil
import os
import numpy as np

import pyvips as pv
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, precision_score, recall_score
from torchvision import transforms

sys.path.append('/mnt/slidl')
from slidl.slide import Slide


def hasInference(slide, inferredClass='IM'):
    foundPrediction = False
    for tileAddress, tileEntry in slide.tileDictionary.items():
        if 'classifierInferencePrediction' in tileEntry:
            if inferredClass in tileEntry['classifierInferencePrediction']:
                foundPrediction = True
                break
    return(foundPrediction)


def extractInferredTiles(slide, outputDir=None, probabilityThreshold=0.95, numTilesToExtractPerClass='all', slideClass=False, otherClasses=False, classesToExtract=False, foregroundLevelThreshold=False, seed=False):
    if seed:
        if type(seed) != int:
            raise ValueError('Seed must be an integer')
        random.seed(seed)
    id = slide.slideFileName

    if not slideClass or not otherClasses:
        raise ValueError('slideClass and otherClasses must not be false for tile extraction')

    # get classes to extract
    extractionClasses = []
    #        if otherClassNames == 'discernFromClassesToExtract':
    #            extraClasses = []

    if not classesToExtract:  # get all predicted classes
        for tileAddress in slide.suitableTileAddresses(foregroundLevelThreshold=foregroundLevelThreshold):
            tileEntry = slide.tileDictionary[tileAddress]
            if 'classifierInferencePrediction' in tileEntry:
                extractionClasses = list(tileEntry['classifierInferencePrediction'].keys())
    else:
        extractionClasses = [classesToExtract]
        for exClass in extractionClasses:
            if not hasInference(slide, exClass):
                raise ValueError(exClass + ' not found in tile dictionary')

    slideToInferredClass = {}
    slideToInferredClass[slideClass] = classesToExtract

    probabilityThresholdDict = {}
    if (type(probabilityThreshold) == int) or (type(probabilityThreshold) == float):
        if (probabilityThreshold <= 0) or (probabilityThreshold > 1):
            raise ValueError('probabilityThreshold must be greater than 0 and less than or equal to 1')
        for extractionClass in extractionClasses:
            probabilityThresholdDict[extractionClass] = probabilityThreshold
    else:
        probabilityThresholdDict = probabilityThreshold

    # Get tiles to extract
    classifierTileAddresses = {extractionClass: [] for extractionClass in extractionClasses}

    suitable_tile_addresses = slide.suitableTileAddresses(foregroundLevelThreshold=foregroundLevelThreshold)
    for address in suitable_tile_addresses:
        for extractionClass in extractionClasses:
            if slide.tileDictionary[address]['classifierInferencePrediction'][extractionClass] >= probabilityThresholdDict[extractionClass]:
                classifierTileAddresses[extractionClass].append(address)

    classifiedTilesToExtract = {}
    if type(numTilesToExtractPerClass) == int:
        if numTilesToExtractPerClass <= 0:
            raise ValueError('If numTilesToExtractPerClass is an integer, it must be greater than 0')
        for extractionClass in extractionClasses:
            if len(classifierTileAddresses[extractionClass]) == 0:
                print('Warning: 0 suitable ' + extractionClass + ' tiles found')
            if len(classifierTileAddresses[extractionClass]) < numTilesToExtractPerClass:
                print('Warning: ' + str(len(classifierTileAddresses[extractionClass])) + ' suitable ' + extractionClass + ' tiles found but requested ' + str(
                    numTilesToExtractPerClass) + ' tiles to extract. Extracting all suitable tiles...')
                classifiedTilesToExtract[slideClass] = classifierTileAddresses[extractionClass]
            else:
                classifiedTilesToExtract[slideClass] = random.sample(classifierTileAddresses[extractionClass], numTilesToExtractPerClass)
    elif numTilesToExtractPerClass == 'all':
        for extractionClass in extractionClasses:
            if len(classifierTileAddresses[extractionClass]) == 0:
                print('Warning: 0 suitable ' + extractionClass + ' tiles found')
            if len(classifierTileAddresses[extractionClass]) > 500:
                print('Warning: ' + str(len(classifierTileAddresses[extractionClass])) + ' suitable ' + extractionClass + ' tiles found')
            classifiedTilesToExtract[slideClass] = classifierTileAddresses[extractionClass]
    elif type(numTilesToExtractPerClass) == dict:
        for ec, tc in numTilesToExtractPerClass.items():
            if ec not in extractionClasses:
                raise Warning('Class ' + ec + ' present as a key in numTilesToExtractPerClass dictionary but absent from the tileDictionary')
        for extractionClass in extractionClasses:
            if len(classifierTileAddresses[extractionClass]) == 0:
                print('Warning: 0 suitable ' + extractionClass + ' tiles found')
            if extractionClass not in numTilesToExtractPerClass:
                raise ValueError(extractionClass + ' not present in the numTilesToExtractPerClass dictionary')
            numTiles = numTilesToExtractPerClass[extractionClass]
            if (type(numTiles) != int) or (numTiles <= 0):
                raise ValueError(extractionClass + ' does not have a positive integer set as its value in the numTilesToExtractPerClass dictionary')
            if len(classifierTileAddresses[extractionClass]) < numTiles:
                print('Warning: ' + str(len(classifierTileAddresses[extractionClass])) + ' suitable ' + extractionClass + ' tiles found but requested ' + str(numTiles) + ' tiles to extract. Extracting all suitable tiles...')
                classifiedTilesToExtract[slideClass] = classifierTileAddresses[extractionClass]
            else:
                classifiedTilesToExtract[slideClass] = random.sample(classifierTileAddresses[extractionClass], numTiles)
    else:
        raise ValueError("numTilesToExtractPerClass must be a positive integer, a dictionary, or 'all'")

    # Create empty class tile directories for extractionClasses with at least one suitable tile
    returnOnlyNumTilesFromThisClass = False
    extractSegmentationMasks = True
    for ec, tte in classifiedTilesToExtract.items():
        if len(tte) > 0:
            try:
                if os.path.exists(os.path.join(outputDir, 'tiles', id, ec)):
                    shutil.rmtree(os.path.join(outputDir, 'tiles', id, ec))
                os.makedirs(os.path.join(outputDir, 'tiles', id, ec), exist_ok=True)
            except:
                raise ValueError(os.path.join(outputDir, 'tiles', id, ec) + ' is not a valid path')

            # Create empty class mask directory (if desired)
            #try:
                #if os.path.exists(os.path.join(outputDir, 'masks')):
                #    shutil.rmtree(os.path.join(outputDir, 'masks'))
                #os.makedirs(os.path.join(outputDir, 'masks', id, ec), exist_ok=True)
            #except:
                #raise ValueError(os.path.join(outputDir, 'masks', id, ec) + ' is not a valid path')

    # Create empty class tile directories
    for cl in otherClasses:
        os.makedirs(os.path.join(outputDir, 'tiles', id, cl), exist_ok=True)
        #os.makedirs(os.path.join(outputDir, 'masks', id, cl), exist_ok=True)

    channel_sums = np.zeros(3)
    channel_squared_sums = np.zeros(3)
    tileCounter = 0
    normalize_to_1max = transforms.Compose([transforms.ToTensor()])

    # Extract tiles
    for ec, tte in classifiedTilesToExtract.items():
        inferredClass = slideToInferredClass[ec]

        if returnOnlyNumTilesFromThisClass:  # and ec == returnOnlyNumTilesFromThisClass:
            return (len(classifierTileAddresses[ec]))

        if len(tte) > 0:
            if extractSegmentationMasks:
                print("Extracting " + str(len(tte)) + " of " + str(len(classifierTileAddresses[inferredClass])) + " " + inferredClass + " tiles and segmentation masks...")
            else:
                print("Extracting " + str(len(tte)) + " of " + str(len(classifierTileAddresses[inferredClass])) + " " + inferredClass + " tiles...")

        for tl in tte:
            area = slide.getTile(tl)
            if foregroundLevelThreshold:
                area.write_to_file(os.path.join(outputDir, 'tiles', id, ec,
                                                id + '_' + ec + '_' + str(slide.tileDictionary[tl]['x']) + 'x_' + str(slide.tileDictionary[tl]['y']) + 'y' + '_' + str(slide.tileDictionary[tl]['height']) + 'tilesize_' + str(
                                                    int(round(slide.tileDictionary[tl]['foregroundLevel']))) + 'foregroundLevel.jpg'), Q=100)
            else:
                area.write_to_file(os.path.join(outputDir, 'tiles', id, ec,
                                                id + '_' + ec + '_' + str(slide.tileDictionary[tl]['x']) + 'x_' + str(slide.tileDictionary[tl]['y']) + 'y' + '_' + str(slide.tileDictionary[tl]['height']) + 'tilesize.jpg'), Q=100)

            tileCounter = tileCounter + 1

            nparea = slide.getTile(tl, writeToNumpy=True)[..., :3]  # remove transparency channel
            nparea = normalize_to_1max(nparea).numpy()  # normalize values from 0-255 to 0-1
            local_channel_sums = np.sum(nparea, axis=(1, 2))
            local_channel_squared_sums = np.sum(np.square(nparea), axis=(1, 2))
            channel_sums = np.add(channel_sums, local_channel_sums)
            channel_squared_sums = np.add(channel_squared_sums, local_channel_squared_sums)

            # Extract segmentation masks
#            mask = slide.getAnnotationTileMask(tl, inferredClass)
#            if foregroundLevelThreshold:
#                mask.save(os.path.join(outputDir, 'masks', id, ec,
#                                       id + '_' + ec + '_' + str(slide.tileDictionary[tl]['x']) + 'x_' + str(slide.tileDictionary[tl]['y']) + 'y' + '_' + str(slide.tileDictionary[tl]['height']) + 'tilesize_' + str(
#                                           int(round(slide.tileDictionary[tl]['foregroundLevel']))) + 'foregroundLevel_mask.gif'))
#            else:
#                mask.save(os.path.join(outputDir, 'masks', id, ec,
#                                       id + '_' + ec + '_' + str(slide.tileDictionary[tl]['x']) + 'x_' + str(slide.tileDictionary[tl]['y']) + 'y' + '_' + str(slide.tileDictionary[tl]['height']) + 'tilesize_mask.gif'))

    if tileCounter == 0:
        print('Warning: 0 suitable annotated tiles found across all classes; making no tile directories and returning zeroes')

    return {'slide': slide.slideFileName,
            'channel_sums': channel_sums,
            'channel_squared_sums': channel_squared_sums,  # np.mean(channel_stds_across_tiles, axis=0).tolist(),
            'num_tiles': tileCounter}

def setInferenceToAnnotation(slide, inferenceLabel='IM', probabilityThreshold=0.95, foregroundLevelThreshold=85) :
    if inferenceLabel is None:
        raise Exception('Inference label that exists within the slide is required.')

    setattr(slide, 'slide.inferenceAnnotationAddresses', [])

    inferenceAnnotationAddresses = []
    for tileAddress in slide.suitableTileAddresses(foregroundLevelThreshold=foregroundLevelThreshold):
        tileEntry = slide.tileDictionary[tileAddress]
        if 'classifierInferencePrediction' in tileEntry:
            if inferenceLabel in tileEntry['classifierInferencePrediction'] and tileEntry['classifierInferencePrediction'][inferenceLabel] >= probabilityThreshold:
                tileEntry['inferenceAnnotation'] = tileEntry['classifierInferencePrediction']
                inferenceAnnotationAddresses.append(tileAddress)
            slide.inferenceAnnotationAddresses = inferenceAnnotationAddresses
    return(slide)

def classifierMetricAtThreshold(slide, classToVisualize, classToThreshold, probabilityThresholds,  metric="accuracy", assignZeroToTilesWithoutAnnotation=True):

#    if not hasattr(slide, 'inferenceAnnotationAddresses'):
#        raise Exception('No inferred annotations set. Run setInferenceToAnnotation on the slide first')

#    if not hasattr(slide, 'classifierPredictionTileAddresses'):
    foundPrediction = False
    classifierPredictionTileAddresses = []
    for tileAddress, tileEntry in slide.tileDictionary.items():
        if 'classifierInferencePrediction' in tileEntry and classToVisualize in tileEntry['classifierInferencePrediction']:
            classifierPredictionTileAddresses.append(tileAddress)
            foundPrediction = True
    if foundPrediction:
        slide.classifierPredictionTileAddresses = classifierPredictionTileAddresses
    else:
        raise ValueError('No classification predictions found in Slide. Use slidl.inferClassifier() to generate them.')

    if type(probabilityThresholds) in [float, int]:
        pT = [probabilityThresholds]
    elif type(probabilityThresholds) == list:
        pT = probabilityThresholds
    else:
        raise ValueError('probabilityThresholds must be an int, float, or list of ints or floats')

    metrics = []

    for probabilityThreshold in pT:
        ground_truths = []
        predictions = []
        for predictionTileAddress in slide.classifierPredictionTileAddresses:

            # ground truth is from the 'inferenceAnnotation'
            if classToThreshold not in slide.tileDictionary[predictionTileAddress]['inferenceAnnotation']:
                if assignZeroToTilesWithoutAnnotation:
                    ground_truths.append(0)
                else:
                    raise ValueError(classToThreshold + ' not found at tile ' + str(predictionTileAddress))

            elif classToThreshold in slide.tileDictionary[predictionTileAddress]['inferenceAnnotation'][classToThreshold] and slide.tileDictionary[predictionTileAddress][
                'inferenceAnnotation'][classToThreshold] >= 0.95:
                ground_truths.append(1)
            else:
                ground_truths.append(0)

            if classToVisualize not in slide.tileDictionary[predictionTileAddress]['classifierInferencePrediction']:
                raise ValueError(classToVisualize + ' not in classifierInferencePrediction at tile ' + str(predictionTileAddress))
            if slide.tileDictionary[predictionTileAddress]['classifierInferencePrediction'][classToThreshold] >= probabilityThreshold:
                predictions.append(1)
            else:
                predictions.append(0)

        if metric == "accuracy":
            metrics.append(accuracy_score(ground_truths, predictions))
        elif metric == "balanced_accuracy":
            metrics.append(balanced_accuracy_score(ground_truths, predictions))
        elif metric == "f1":
            metrics.append(f1_score(ground_truths, predictions))
        elif metric == "precision":
            metrics.append(precision_score(ground_truths, predictions))
        elif metric == "recall":
            metrics.append(recall_score(ground_truths, predictions))
        else:
            raise ValueError("metric must be one of: 'accuracy', 'balanced_accuracy', 'f1', 'precision', or 'recall'")

    if len(metrics) > 1:
        return metrics
    else:
        return metrics[0]
