#!/bin/python

import os
import sys
import glob
import argparse
import traceback
import warnings
import time
from datetime import date
import csv
import copy
import shutil
import re
from pathlib import Path


import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import torchvision

from torchvision import datasets, models, transforms

import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import resize
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, precision_score, recall_score, roc_auc_score, roc_curve

import pandas as pd
import random
import pickle
import math

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import cm
from matplotlib.ticker import ScalarFormatter, NullFormatter
from skimage.transform import resize

import pyvips as pv

sys.path.append('/mnt/slidl')
from slidl.slide import Slide
from slidl.analysis import Analysis
from slidl.processor import Processor
from slidl.models.tissuedetector import tissueDetector


def parse_boolean(string):
	string = string.lower()
	if string in ["true", "yes", "1"]:
		return True
	return False


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--model_dir', type=str, required=True, help='Trained model, output by train_tissue.py')
parser.add_argument('--labels', default='IM SQBS', required=False, nargs='*',  help='Slide labels default: IM SQBS')
parser.add_argument('--prob', default=0.95, type=float,  help='Tile probability, default: 0.95')
parser.add_argument('--pml_dir', required=True, help="Directory for all slides to infer classes (not annotated)")

## Here for convinience but will be overwritten where possible
parser.add_argument('--tlt', default=0.65, type=float,  help='tissue level threshold, default: 0.65.')
parser.add_argument('--fgt', default=85, type=float,  help='foreground threshold, default: 85. This will be overwritten by info in the pml lib_dict file')
parser.add_argument('--tile_size', default=224, type=int, help='Tile size in px, default: 224.  This will be overwritten by info in the pml lib_dict file')

args = parser.parse_args()
print(args)

today = date.today()

output_dir = os.path.join(args.model_dir, 'applied_classification')
if not os.path.exists(output_dir):
    os.makedirs(output_dir)
    os.makedirs(os.path.join(output_dir, 'pml'))

cohorts = pickle.load(open(glob.glob(os.path.join(args.model_dir, 'classification_cohorts_*'))[0], 'rb'))
modelPath = glob.glob(os.path.join(args.model_dir, 'classification_best_model*'))[0]

means_and_stds = cohorts['train_val_norm']

tlt = args.tlt
fgt = args.fgt
pml_dir = args.pml_dir
params_file = glob.glob(os.path.join(args.model_dir, 'training_params.csv'))[0]
if os.path.exists(params_file):
    params = pd.read_csv(params_file)
    labels = re.sub(r"\[|\]|'| ", "", params.loc[0,'labels']).split(",")

    #if args.pred_label not in labels:
    #    print(args.pred_label + " was not in the set of labels model was trained for: " + ", ".join(labels))
    #    quit()

    tile_size = params.loc[0,'tile_size'].item()
    ann_params = pickle.load(open(os.path.join(params.loc[0,'tiles'], 'annotation_info.p'),'rb'))
    #tlt = False if ann_params['tissueLevelThreshold'] is None else ann_params['tissueLevelThreshold']
    fgt = False if ann_params['foregroundLevelThreshold'] is None else ann_params['foregroundLevelThreshold']
    #overlap = ann_params['overlap']
    #pml_dir = os.path.join(params.loc[0,'tiles'], 'tile_dict')
else:
    print("Warning: no training_params.csv file available. Please be sure your annotation, training and inference parameters match. There are no checks.")
    tlt = args.tlt
    fgt = args.fgt
    tile_size = args.tile_size
    labels = args.labels
    #overlap = args.overlap
    pml_dir = args.pml_dir
    if pml_dir is None or tlt is None or fgt is None or tile_size is None or labels is None or overlap is None:
        parser.print_help()
        quit()

print('*** PMLs for cases: ', pml_dir)
model_cases = { key: cohorts['cases'][key] for key in ['train','test','val'] }
#exclude_cases = model_cases['train'] + model_cases['val']

pml_cases = glob.glob(os.path.join(pml_dir, '*.pml'))
#print(len(pml_cases))

# Can't exclude my training cases for the next step
#for case in exclude_cases:
#    ex = [s for s in pml_cases if os.path.basename(case) in s][0]
#    pml_cases.remove(ex)
print('Total cases to infer:', len(pml_cases))


dataTransforms = transforms.Compose([
    transforms.Resize(tile_size),
    transforms.ToTensor(),
    transforms.Normalize(means_and_stds['channel_means'], means_and_stds['channel_stds'])])

trainedModel = models.vgg19_bn(pretrained=False)
num_ftrs = trainedModel.classifier[6].in_features
trainedModel.classifier[6] = nn.Linear(num_ftrs, len(args.labels))
trainedModel.load_state_dict(torch.load(modelPath))


tile_counts = pd.DataFrame(index=[ Path(pml).stem for pml in pml_cases], data={"ge"+str(args.prob):0, "ge0.5":0, 'total_tiles':0})

slide_inf_file = os.path.join(output_dir, 'inferred_slides.p')
if os.path.exists(slide_inf_file):
	infer_dict = pickle.load(open(slide_inf_file, 'rb'))
else:
	infer_dict = {}

primary_label=args.labels[0]
print("Inferring classification for ", primary_label)

labels=args.labels
labels.sort()

start_time = time.time()
for case in pml_cases:
    print('Case: ', case)
    pml_file = [s for s in pml_cases if os.path.basename(case) in s][0]
    print('PML: ', pml_file)

    if case in infer_dict:
        print(case + " already inferred, skipping")
        tile_counts.at[os.path.basename(case),"ge"+str(args.prob)] = infer_dict[case]["ge"+str(args.prob)]
        #tile_counts.at[os.path.basename(case),"ge0.5"] = infer_dict[case]['ge0.5'] 
        tile_counts.at[os.path.basename(case),'total_tiles'] = infer_dict[case]['total_tiles']
        continue 

    try:
        slidl_slide = Slide(pml_file, verbose=True)
        slidl_slide.inferClassifier(trainedModel, classNames=labels, dataTransforms=dataTransforms, tissueLevelThreshold=False, foregroundLevelThreshold=fgt)
        slidl_slide.save(folder=os.path.join(output_dir, 'pml'))

        #print('Tiles>0.95 for '+primary_label, slidl_slide.numTilesAboveClassPredictionThreshold(primary_label, args.prob-0.01), ' total tiles:', slidl_slide.getTileCount(foregroundLevelThreshold=fgt, tissueLevelThreshold=tlt))
        #tile_counts.at[os.path.basename(case),"ge"+str(args.prob)] = slidl_slide.numTilesAboveClassPredictionThreshold(primary_label, args.prob)
        #tile_counts.at[os.path.basename(case),"ge0.5"] = slidl_slide.numTilesAboveClassPredictionThreshold(primary_label, 0.50-0.01)
        tile_counts.at[os.path.basename(case),'total_tiles'] = slidl_slide.getTileCount(foregroundLevelThreshold=fgt, tissueLevelThreshold=tlt)

        infer_dict[case] = { "ge"+str(args.prob): tile_counts.at[os.path.basename(case),"ge"+str(args.prob)], 'total_tiles': tile_counts.at[os.path.basename(case),'total_tiles'] }

        slidl_slide.visualizeClassifierInference(primary_label, folder=os.path.join(output_dir, primary_label))
        plt.close('all')
	
        tile_counts.to_csv(os.path.join(output_dir, 'tile_counts.csv'), index_label = 'Case', mode='w')

        pickle.dump(infer_dict, open(slide_inf_file,'wb'))
    except Exception as e:
        print(e.__class__, " error occurred")
        traceback.print_tb(e.__traceback__)
        traceback.print_exc()

pickle.dump(infer_dict, open(slide_inf_file,'wb'))

tile_counts.to_csv(os.path.join(output_dir, 'tile_counts.csv'), index_label = 'Case', mode='w')
time_elapsed = time.time() - start_time
print('Complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
print('** Finished **')

