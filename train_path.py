#!/bin/python

import os
import sys
import glob
import argparse
import traceback
import warnings
import time
import datetime
from datetime import date
import csv
import copy
import collections

from tqdm import tqdm

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import torchvision

from torchvision import datasets, models, transforms

import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import resize
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, precision_score, recall_score, roc_auc_score, roc_curve

import pandas as pd
import random
import pickle
import math

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import cm
from matplotlib.ticker import ScalarFormatter, NullFormatter
from skimage.transform import resize

import pyvips as pv

from openslide import OpenSlide

sys.path.append('/mnt/pathml')
from pathml.slide import Slide
from pathml.analysis import Analysis
from pathml.processor import Processor
from pathml.models.tissuedetector import tissueDetector


print("PyTorch version:", torch.__version__)
print("pyvips version:", pv.__version__)



parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--tiles_dir', type=str, required=True, help='Tiled slide images directory, output by extract_tiles.py')
parser.add_argument('--slide_list',type=str, required=True, help='csv file listing slides')
parser.add_argument('--output_dir', type=str, required=True, help='')
parser.add_argument('--run_name', type=str, required=False, default=None, help='Name this training run')
parser.add_argument('--epochs', type=int, default=30, help='Number of epochs to train')
parser.add_argument('--datasize',type=float, default=0.4, help='Ratio of data to use, 0.1-1')
parser.add_argument('--labels', default='path', choices=['path','dysplasia', 'progression'], help='pathology, dysplasia or progression (path:dysplasia:prog)')

args = parser.parse_args()
print(args)

today = date.today()

if args.datasize < 0.1 or args.datasize > 1.0:
	exit("Datasize must be from 0.1-1")

if not args.run_name:
	args.run_name = "run_" + today.strftime("%Y%m%d") + "_epochs_" + str(args.epochs)
else:
	args.run_name = args.run_name + '_' + today.strftime("%Y%m%d") + "_epochs_" + str(args.epochs)

if not os.path.exists(args.slide_list):
	print("File not found: ", args.slide_list)
	quit()

results_path = os.path.join(args.output_dir, args.run_name)
if not os.path.isdir(results_path):
	os.makedirs(results_path)

level=0 # extract tiles at highest resolution
mult=1 # no scaling
tile_size=224

# Set up the test train list of pml files
ndbe = list()
lgd = list()
hgd = list()

with open(args.slide_list, newline='') as csvfile:
	dialect = csv.Sniffer().sniff(csvfile.read(1024))
	csvfile.seek(0)
	reader = csv.reader(csvfile, dialect)
	for row in reader:
		if len(row) < 5:
			print("csv file not formatted as expected: PID,Status,Pathology,WSI name,WSI base")
			quit()
        
		if (row[0].__eq__('PatientID')):
			continue

		pid, status, path, wsi_name, wsi_base = row
		wsi_base = os.path.join(args.tiles_dir, 'tiles',  wsi_base)
		if not os.path.exists(wsi_base):
			continue 

		
		if path == 'NDBE' or path == 'ID':
			ndbe.append(wsi_base)
		if path == 'LGD':
			lgd.append(wsi_base)
		if path == 'HGD' or path == 'IMC':
			hgd.append(wsi_base)



seed = 366
random.seed(seed)


random.shuffle(ndbe)
random.shuffle(lgd)
random.shuffle(hgd)

print('NDBE:',len(ndbe),' LGD:',len(lgd),' HGD:',len(hgd))
# downsize
if args.datasize < 1.0:
	print("Downsizing sets to: ", args.datasize*100, "%")
	x, ndbe = train_test_split(ndbe, test_size=args.datasize, random_state=42)
	x, lgd = train_test_split(lgd, test_size=args.datasize, random_state=42)
	x, hgd = train_test_split(lgd, test_size=args.datasize, random_state=42)

ndbe = random.choices(ndbe, k=len(hgd))
lgd = random.choices(lgd, k=len(hgd))
print('NDBE:',len(ndbe),' LGD:',len(lgd),' HGD:',len(hgd))
sys.stdout.flush()

if args.labels == 'path':
	# 70%, 15%, 15% splits
	ndbe_train, ndbe_test = train_test_split(ndbe, test_size=0.30, random_state=42)
	ndbe_test, ndbe_val = train_test_split(ndbe_test, test_size=0.5, random_state=42)

	lgd_train, lgd_test = train_test_split(lgd, test_size=0.30, random_state=42)
	lgd_test, lgd_val = train_test_split(lgd_test, test_size=0.5, random_state=42)

	hgd_train, hgd_test = train_test_split(hgd, test_size=0.30, random_state=42)
	hgd_test, hgd_val = train_test_split(hgd_test, test_size=0.5, random_state=42)

	train_cases = ndbe_train + lgd_train + hgd_train
	test_cases = ndbe_test + lgd_test + hgd_test
	val_cases = ndbe_val + lgd_val + hgd_val

elif args.labels == 'progression':
	non_prog = ndbe + lgd
	prog = hgd
    
	nonp_train, nonp_test = train_test_split(non_prog, test_size=0.3, random_state=42)
	nonp_test, nonp_val = train_test_split(non_prog, test_size=0.5, random_state=42)

	prog_train, prog_test = train_test_split(prog, test_size=0.3, random_state=42)
	prog_test, prog_val = train_test_split(prog, test_size=0.5, random_state=42)
    
	train_cases = nonp_train + prog_train
	test_cases = nonp_test + prog_test
	val_cases = nonp_val + prog_val
    
	print('NP:', len(non_prog), ' P:', len(prog)) 
elif args.labels == 'dysplasia':
	dysp = lgd + hgd
	ndbe_train, ndbe_test = train_test_split(ndbe, test_size=0.30, random_state=42)
	ndbe_test, ndbe_val = train_test_split(ndbe_test, test_size=0.5, random_state=42)

	dysp_train, dysp_test = train_test_split(dysp, test_size=0.30, random_state=42)
	dysp_test, dysp_val = train_test_split(dysp_test, test_size=0.5, random_state=42)

	train_cases = ndbe_train + dysp_train
	test_cases = ndbe_test + dysp_test
	val_cases = ndbe_val + dysp_val


warnings.simplefilter('ignore')

cache_file = os.path.join(args.tiles_dir, 'trainval_channel_means_and_stds.p')
if os.path.exists(cache_file):
	print("Load pre-cached tile info:",cache_file)
	# Reload mean and std data for normalization
	means_and_stds = pickle.load(open(cache_file, 'rb'))
	
else:
	print("channel means not available, run extract_tiles.py script first")
	quit()

slide_info = means_and_stds['slide_info']
global_channel_sums = means_and_stds['channel_sums'] 
global_channel_squared_sums = means_and_stds['channel_sq_sums']
global_tile_count = means_and_stds['tile_counts'] 
class_tiles = means_and_stds['class_tiles']

all_tiles = list(map(lambda x: x['totalTiles'], means_and_stds['slide_info'].values()))
#print(all_tiles)

total_pixels_per_channel = global_tile_count * tile_size * tile_size
global_channel_means = np.divide(global_channel_sums, total_pixels_per_channel)
global_channel_squared_means = np.divide(global_channel_squared_sums, total_pixels_per_channel)
global_channel_variances = np.subtract(global_channel_squared_means, np.square(global_channel_means))
global_channel_stds = np.sqrt(global_channel_variances * (total_pixels_per_channel / (total_pixels_per_channel-1)))

print('Channel means: ', global_channel_means)
print('Channel std dev: ', global_channel_squared_means)

class_names = list(class_tiles.keys())
class_names.sort() # class names MUST be sorted alphabetically
print('Class names: ', class_names)

# the tile size that we will resize to because vgg19 expects it
patch_size = 224
# Choose minibatch size according to your GPU. 
# Ours was run on a GeForce RTX 2080 Ti and these are the best sizes for us according to architecture
batch_size = 48
# Select number of epochs (very adjustable, depends on when the model converges, start larger than you'd think!)
num_epochs = args.epochs

# Define training and validation augmentations
# We are introducing strong HSV color jittering to account 
# for the different labs the images were scanned at
data_transforms = {
	'train': transforms.Compose([
		transforms.Resize(patch_size),
		transforms.RandomVerticalFlip(),
		transforms.RandomHorizontalFlip(),
		transforms.ColorJitter(brightness=0, contrast=0, saturation=1, hue=.5), # can be added and shown for example
		transforms.ToTensor(),
		transforms.Normalize(global_channel_means, global_channel_squared_means) # can be added and shown for example
    ]),
	'val': transforms.Compose([
		transforms.Resize(patch_size),
			transforms.ToTensor(),
			transforms.Normalize(global_channel_means, global_channel_squared_means)
		]),
}
sys.stdout.flush()

print("PyTorch datasets from ", os.path.join(results_path, 'tiles'))

# Create train and val PyTorch datasets
train_dataset = torch.utils.data.ConcatDataset([datasets.ImageFolder(train_case, data_transforms['train']) for train_case in train_cases])
val_dataset = torch.utils.data.ConcatDataset([datasets.ImageFolder(val_case, data_transforms['val']) for val_case in val_cases])

print(train_dataset)
targets = []
for _, target in train_dataset:
    targets.append(target)
targets = torch.tensor(targets)

# Compute samples weight (each sample should get its own weight)
class_sample_count = torch.tensor( [(targets == t).sum() for t in torch.unique(targets, sorted=True)] )
weight = 1.0 / class_sample_count.float()
samples_weight = torch.tensor([weight[t] for t in targets])

sampler = torch.utils.data.WeightedRandomSampler(weights=samples_weight, num_samples=len(samples_weight), replacement=True)

image_datasets = {'train': train_dataset, 'val': val_dataset}
dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'val']}
    
dataloaders = {}
# note: here is where imbalanced dataset sampling happens if used, using [LINK] package
#dataloaders['train'] = torch.utils.data.DataLoader(image_datasets['train'], batch_size=batch_size, num_workers=16, shuffle=True)
dataloaders['train'] = torch.utils.data.DataLoader(image_datasets['train'], batch_size=batch_size, num_workers=16, sampler=sampler)
dataloaders['val'] = torch.utils.data.DataLoader(image_datasets['val'], batch_size=batch_size, num_workers=16, shuffle=True)


# Iterate DataLoader and check class balance for each batch
for i, (x, y) in enumerate(dataloaders['train']):
    print("batch index {}, 0/1/2: {}/{}/{}".format(i, (y == 0).sum(), (y == 1).sum(), (y == 2).sum()))
    if i >= 50: break
	

# Check for GPU
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

if torch.cuda.is_available():
    print("Training on GPU")
else:
    print("Training on CPU")
    
# Set up pretraining (adjustable! show how training from scratch takes much more training time)
model_ft = models.vgg19_bn(pretrained=True)
# model_ft = models.inception_v3(pretrained=True)
num_ftrs = model_ft.classifier[6].in_features
model_ft.classifier[6] = nn.Linear(num_ftrs, len(class_names))

#print('Num features:',num_ftrs)

# Set up use of parallel processing if multiple gpus are present; otherwise set up normally
if torch.cuda.device_count() > 1:
	print("*** Using", torch.cuda.device_count(), "GPUs!")
	model_ft = nn.DataParallel(model_ft)
model_ft = model_ft.to(device)

# Select loss function (adjustable!)
criterion = nn.CrossEntropyLoss()

# Select optimization function (adjustable! try Adam as well)
optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)
#optimizer_ft = optim.Adam(model_ft.parameters(), lr=0.001)

# Set up learning rate decay (very very adjustable)
# Decay LR by a factor of 0.1 every 7 epochs
exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)

def train_model(model, criterion, optimizer, classes, scheduler=False, num_epochs=30):
	since = time.time()
	best_model_state_dict = copy.deepcopy(model.state_dict())
	best_accuracy = 0.0

	# Create dictionary to store performance metrics at each epoch of training
	learning_stats = {'train': [], 'val': []}
	
	#Iterate over epochs
	for epoch in range(num_epochs):
		print('Epoch {}/{}'.format(epoch, num_epochs - 1))
		print('-'*10)

		# Train/Val phase per epoch
		for phase in ['train', 'val']:
			if phase == 'train':
				model.train() # Set model to training mode, weights can be adjusted
			else:
				model.eval() # Set model to evaluate mode, weights cannot be adjusted

			epoch_ground_truth = []
			epoch_predictions = []
			running_loss = 0.0
			correct_pred = {classname: 0 for classname in classes}
			total_pred = {classname: 0 for classname in classes}

			# Iterate over data
			for inputs, labels in tqdm(dataloaders[phase]):
				inputs = inputs.to(device)
				labels = labels.to(device)
				
				pickle.dump({'epoch':epoch, 'phase':phase, 'labels':labels.tolist()}, open(os.path.join(results_path, 'labels.p'), 'ab'))
				
				with torch.set_grad_enabled(phase == 'train'):
					outputs = model(inputs)
					_, preds = torch.max(outputs, 1)
					loss = criterion(outputs, labels)

					# backward pass only during training phase
					if phase == 'train':
						loss.backward() # apply backprop
						optimizer.step()

					# measure performance statistics
					epoch_ground_truth = epoch_ground_truth + labels.data.tolist()
					epoch_predictions = epoch_predictions + preds.tolist()
					running_loss += loss.item() * inputs.size(0)

					for label, prediction in zip(labels, preds):
						total_pred[classes[label]] += 1
						if label == prediction:
							correct_pred[classes[label]] += 1

				# Increment learning rate recay scheduler if train phase
				if scheduler:
					if phase == 'train':
						scheduler.step()

				# Compute epoch-wide performance metrics
				epoch_loss = running_loss / dataset_sizes[phase]
				epoch_acc = accuracy_score(epoch_ground_truth, epoch_predictions)
				epoch_weighted_acc = balanced_accuracy_score(epoch_ground_truth, epoch_predictions)  # accuracy accounting for class imbalance
				epoch_weighted_rec = recall_score(epoch_ground_truth, epoch_predictions, average='weighted')  # average recall accounting for class imbalance
				epoch_weighted_prec = precision_score(epoch_ground_truth, epoch_predictions, average='weighted')  # average precision accounting for class imbalance
				epoch_weighted_f1 = f1_score(epoch_ground_truth, epoch_predictions, average='weighted')  # average F1 score accounting for class imbalance
            
				learning_stats[phase].append({
					'epoch': epoch,
					'loss': epoch_loss, 
					'accuracy': epoch_acc, 
					'weighted_accuracy': epoch_weighted_acc,
					'weighted_precision': epoch_weighted_prec,
					'weighted_recall': epoch_weighted_rec,
					'weighted_f1': epoch_weighted_f1,
					'class_accuracy': correct_pred,
					'total_predictions': total_pred
				})

			print('Phase Loss: {:.4f} Acc: {:.4f} Weighted Acc: {:.4f} Weighted Pre: {:.4f} Weighted Rec: {:.4f} Weighted F1: {:.4f}')
			print(phase, epoch_loss, epoch_acc, epoch_weighted_acc, epoch_weighted_prec, epoch_weighted_rec, epoch_weighted_f1)

			for classname, correct_count in correct_pred.items():
				accuracy = 100 * float(correct_count) / total_pred[classname]
				print("Accuracy for class {:5s} is: {:.1f} %".format(classname,accuracy))

			# deep copy the model if it shows the best validation performance
			if (phase == 'val') and (epoch_acc > best_accuracy): # MIGHT WANT TO USE SOMETHING OTHER THAN ACCURACY TO MEASURE BEST EPOCH
				best_accuracy = epoch_acc
				best_model_state_dict = copy.deepcopy(model.state_dict())
				# sometimes it's useful to save every epoch's model

	time_elapsed = time.time() - since
	print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
	print('Best val accuracy: {:4f}'.format(best_accuracy))

	# load best model weights
	return best_model_state_dict, learning_stats


model_ft, learningStats = train_model(model_ft, criterion, optimizer_ft, class_names, scheduler=exp_lr_scheduler, num_epochs=num_epochs)

learning_stats_file = os.path.join(results_path, 'classification_learning_stats_' + today.strftime("%Y%m%d") +'.p')
pickle.dump(learningStats, open(learning_stats_file, 'wb'))

class_model_file = os.path.join(results_path, 'classification_best_model_' + today.strftime("%Y%m%d") + '_ft.pt')
torch.save(model_ft, class_model_file)

learningStats = pickle.load(open(learning_stats_file, "rb"))
trainLoss = [epoch["loss"] for epoch in learningStats['train']]
valLoss = [epoch["loss"] for epoch in learningStats['val']]
trainAcc = [epoch["weighted_accuracy"] for epoch in learningStats['train']]
valAcc = [epoch["weighted_accuracy"] for epoch in learningStats['val']]
numEpochs = len(learningStats['train'])

print("**** Finished ****")


