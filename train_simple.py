#!/bin/python

import os
import sys
import glob
import argparse
import traceback
import warnings
import time
import datetime
from datetime import date
import csv
import copy
import collections

from tqdm import tqdm

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import torchvision

from torchvision import datasets, models, transforms

import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import resize
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, precision_score, recall_score, roc_auc_score, roc_curve

from pathlib import Path
import pandas as pd
import random
import pickle
import math

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import cm
from matplotlib.ticker import ScalarFormatter, NullFormatter
from skimage.transform import resize

import pyvips as pv

from openslide import OpenSlide

sys.path.append('/mnt/slidl')
from slidl.slide import Slide
from slidl.analysis import Analysis
from slidl.processor import Processor
from slidl.models.tissuedetector import tissueDetector

from classification_function import train_classification_model

print("PyTorch version:", torch.__version__)
print("pyvips version:", pv.__version__)

def parse_boolean(string):
	string = string.lower()
	if string in ["true", "yes", "1"]:
		return True
	return False


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--tiles_dir', type=str, required=True, help='Tiled slide images directory, output by extract_tiles.py')
parser.add_argument('--output_dir', type=str, required=True, help='')
parser.add_argument('--run_name', type=str, required=False, default=None, help='Name this training run')
parser.add_argument('--epochs', type=int, default=30, help='Number of epochs to train')
parser.add_argument('--labels', default='IM SQBS', nargs='*',  help='Slide labels default: IM SQBS')
parser.add_argument('--batchsize', default=48, type=int, help='Batch size')
parser.add_argument('--pretrain', default='True', type=parse_boolean, help='Flag for using pretrained network (vgg19 currently)')
parser.add_argument('--splits', type=str, required=False, help='TSV file with the train/test/val split for cases. If not used will be randomly generated')

args = parser.parse_args()
print(args)

today = date.today()

if not args.run_name:
	args.run_name = "run_" + today.strftime("%Y%m%d") + "_epochs_" + str(args.epochs) + 'vgg19pretrain_' + str(args.pretrain) + 'batch' + str(args.batchsize) 
else:
	args.run_name = args.run_name + '_' + today.strftime("%Y%m%d") + "_epochs_" + str(args.epochs) + 'vgg19pretrain_' + str(args.pretrain) + 'batch' + str(args.batchsize)

results_path = os.path.join(args.output_dir, args.run_name)
if not os.path.isdir(results_path):
	os.makedirs(results_path)

level=0 # extract tiles at highest resolution
mult=1 # no scaling
tile_size=224 # get this from the tiles?

# Set up the test train list of pml files

seed = 366
random.seed(seed)

warnings.simplefilter('ignore')

cache_file = os.path.join(args.tiles_dir, 'trainval_channel_means_and_stds.p')
per_case_file = os.path.join(args.tiles_dir, 'tile_counts.p')
if os.path.exists(cache_file):
	print("Load pre-cached tile info:",cache_file)
	# Reload mean and std data for normalization
	per_case = pickle.load(open(per_case_file, 'rb'))
	means_and_stds = pickle.load(open(cache_file, 'rb'))
else:
	print("channel means not available, run extract_tiles.py script first")
#	quit()


#cases = glob.glob(os.path.join(args.tiles_dir, '*', '*'))

#if args.splits is None:
 	# 70%, 15%, 15% splits
#train_cases, val_cases = train_test_split(cases, test_size=0.30, random_state=42)
#test_cases, val_cases = train_test_split(test_cases, test_size=0.5, random_state=42)
#print(len(train_cases), ' ', len(val_cases))
#else:
#	df = pd.read_csv(args.splits, sep=',', header=0)
	#print(df.head())
	#print(df.columns.tolist())
#	train_cases = list(map(lambda sample: os.path.join(args.tiles_dir, 'tiles', sample), df.loc[:,"Train"].tolist()))
#	test_cases = list(map(lambda sample: os.path.join(args.tiles_dir, 'tiles', sample), df.loc[:,"Test"].tolist()))
#	val_cases = list(map(lambda sample: os.path.join(args.tiles_dir, 'tiles', sample), df.loc[:,"Val"].tolist()))


class_names = args.labels
class_names.sort() # class names MUST be sorted alphabetically
print('Class names: ', class_names)
# Choose minibatch size according to your GPU. 
# Ours was run on a GeForce RTX 2080 Ti and these are the best sizes for us according to architecture
batch_size = args.batchsize 
# Select number of epochs (very adjustable, depends on when the model converges, start larger than you'd think!)
num_epochs = args.epochs

# Define training and validation augmentations
# We are introducing strong HSV color jittering to account 
# for the different labs the images were scanned at
data_transforms = {
	'train': transforms.Compose([
		transforms.RandomResizedCrop(tile_size),
		#transforms.Resize(tile_size),
		#transforms.RandomVerticalFlip(),
		transforms.RandomHorizontalFlip(),
		#transforms.ColorJitter(brightness=0, contrast=0, saturation=1, hue=.5), # can be added and shown for example
		transforms.ToTensor(),
		transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
	'val': transforms.Compose([
		transforms.Resize(tile_size),
		transforms.CenterCrop(tile_size),
		transforms.ToTensor(),
		transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
		]),
}

sys.stdout.flush()


print("PyTorch datasets from ", args.tiles_dir)

# Create train and val PyTorch datasets
image_datasets = {x: datasets.ImageFolder(os.path.join(args.tiles_dir, x), data_transforms[x]) for x in ['train', 'val']}
dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'val']}
print('dataset sizes:', dataset_sizes)
    
dataloaders = {}
#sampler = torch.utils.data.WeightedRandomSampler(weights=samples_weight, num_samples=len(samples_weight), replacement=True)

dataloaders['train'] = torch.utils.data.DataLoader(image_datasets['train'], batch_size=batch_size, num_workers=16, shuffle=True)
#dataloaders['train'] = torch.utils.data.DataLoader(image_datasets['train'], batch_size=batch_size, drop_last=True, num_workers=16, sampler=sampler)
dataloaders['val'] = torch.utils.data.DataLoader(image_datasets['val'], batch_size=batch_size, num_workers=16, shuffle=True)


#image_datasets = {x: datasets.ImageFolder(os.path.join(args.tiles_dir, x),data_transforms[x]) for x in ['train', 'val']}
#dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=args.batchsize, shuffle=True, num_workers=16) for x in ['train', 'val']}
#class_names = image_datasets['train'].classes


def visualize_batch(inp, title=None):
    inp = inp.numpy().transpose((1, 2, 0))
    mean = means_and_stds['channel_means']
    std = means_and_stds['channel_stds']
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    #plt.pause(0.001)
    plt.savefig(os.path.join(results_path, 'visualize_augmentation.png'))


inputs, classes = next(iter(dataloaders['train']))
out = torchvision.utils.make_grid(inputs)
#visualize_batch(out)

# Check for GPU
#device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


start_time = time.time()

# Set up pretraining (adjustable! show how training from scratch takes much more training time)
print('Classes:', class_names)
#model_ft = models.vgg19_bn(pretrained=args.pretrain)
#num_ftrs = model_ft.classifier[6].in_features
#model_ft.classifier[6] = nn.Linear(num_ftrs, len(class_names))

model_ft = models.resnet18(pretrained=True)
num_ftrs = model_ft.fc.in_features
model_ft.fc = nn.Linear(num_ftrs, len(class_names))


# Set up use of parallel processing if multiple gpus are present; otherwise set up normally
#device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
#if torch.cuda.device_count() > 1:
#    model_ft = nn.DataParallel(model_ft)
#model_ft = model_ft.to(device)

# Select loss function (adjustable!)
criterion = nn.CrossEntropyLoss()
# Select optimization function (adjustable! try Adam as well)
optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)
# Set up learning rate decay (very very adjustable)
exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)

# Train!
model_ft, learningStats = train_classification_model(model_ft, dataloaders, dataset_sizes, criterion, optimizer_ft, exp_lr_scheduler, num_epochs=num_epochs, classes=class_names)


learning_stats_file = os.path.join(results_path, 'classification_learning_stats_' + today.strftime("%Y%m%d") +'.p')
pickle.dump(learningStats, open(learning_stats_file, 'wb'))

class_model_file = os.path.join(results_path, 'classification_best_model_' + today.strftime("%Y%m%d") + '_ft.pt')
torch.save(model_ft, class_model_file)

time_elapsed = time.time() - start_time
print('Complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

trainLoss = [epoch["loss"] for epoch in learningStats['train']]
valLoss = [epoch["loss"] for epoch in learningStats['val']]
trainAcc = [epoch["weighted_accuracy"] for epoch in learningStats['train']]
valAcc = [epoch["weighted_accuracy"] for epoch in learningStats['val']]
numEpochs = len(learningStats['train'])

fig, (ax1, ax2) = plt.subplots(2)
fig.suptitle('Classification')
ax1.plot(np.arange(numEpochs)+1,trainAcc,'bo-.',label="Training",alpha=0.6,markersize=4)
ax1.plot(np.arange(numEpochs)+1,valAcc,'go-',label="Validation",markersize=4)
ax1.axhline(y=np.max(valAcc),color="r",alpha=0.4)
ax1.set(ylabel="Weighted accuracy")
ax1.label_outer()
ax2.plot(np.arange(numEpochs)+1,trainLoss,'bo-.',label="Training",alpha=0.6,markersize=4)
ax2.plot(np.arange(numEpochs)+1,valLoss,'go-',label="Validation",markersize=4)
ax2.set(xlabel="Epoch", ylabel="Loss")
fig.set_size_inches(7,9)
plt.legend()
plt.savefig(os.path.join(results_path, 'classification_learning_curves.png'))
plt.show(block=False)

print("**** Finished ****")


