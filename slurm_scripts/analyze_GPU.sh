#!/bin/bash

#! Give your job a name
#SBATCH -J analyze_network
#! How many cores per task?
#SBATCH --cpus-per-task=4
#! How much memory do you need?
#SBATCH --mem=30G
#! How much wallclock time will be required?
#SBATCH --time=12:00:00
#! What types of email messages do you wish to receive?
#SBATCH --mail-type=END,FAIL
#! Specify your email address here otherwise you won't recieve emails!
#SBATCH --mail-user=sk875@cam.ac.uk
#! Uncomment this to prevent the job from being requeued (e.g. if
#! interrupted by node failure or system downtime):
##SBATCH --no-requeue
#! Make sure to use the gpunodes partition
#SBATCH -p gpunodes
#! Specify a GRES (generic resource) of type gpu and how many you want
#SBATCH --gres gpu:1
#SBATCH --output=logs/analyze_network-%j.out


SCRATCH='/scratchb/stlab-icgc/users/killco01'
SCRIPT='be_imagepath/infer_analyze.py'
#SING='slide_proc_pytorch1.8.simg'
SING='slide_proc.simg'

usage() {                                 # Function: Print a help message.
  echo "Usage: $0 [ -r results dir (trained model) REQ ] [ -p label to predict (REQ) ]
The following options are only required if the training_params.csv file does not exist in the model directory:
[ -m PML dir  ] [ -l labels (IM,SQBS) ] [ -o overlap (0-1.0) ] " 1>&2  
}
exit_abnormal() {                         # Function: Exit with error.
  usage
  exit 1
}

case=""
while getopts ":r:p:l:m:o:c:t:" options; do
	echo "Option is $options $OPTARG"
        case "${options}" in
                r)
                        modeldir="${OPTARG}"
			modeldir="--results_dir /mnt/${modeldir}"
                        ;;
		p)
			predlabel="--pred_label ${OPTARG}"
			;;
		m)
			pmldir="${OPTARG}"
			pmldir="--pml_dir /mnt/${pmldir}"
			;;
		
		l)
                        labels=$(echo "$OPTARG" | tr "," "\n")
                        labels="--labels ${labels}"
                        ;;
		o)
			overlap="${OPTARG}"
			overlap="--overlap ${overlap}"
			;;
		c)
			case="${OPTARG}"
			case="--case ${case}"
			echo $case
			;;
		t)
			tlt="--tlt ${OPTARG}"
			;;
			

        esac
done

if [ "$modeldir" == "" ] || [ "$predlabel" == "" ]; then
        exit_abnormal
fi

# --nv mounts Nvidia CUDA libraries
date
singularity exec \
  --nv \
  --bind ${SCRATCH}/singularity_recipe:/mnt ${SING} \
  python3 /mnt/${SCRIPT} \
  $modeldir $predlabel $tlt $case $pmldir $labels $overlap 
date
