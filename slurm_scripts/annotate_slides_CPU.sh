#!/bin/bash

#! Give your job a name
#SBATCH -J annotate_extract_tiles
#! How many cores per task?
#SBATCH --cpus-per-task=4
#! How much memory do you need?
#SBATCH --mem=20G
#! How much wallclock time will be required?
#SBATCH --time=48:00:00
#! What types of email messages do you wish to receive?
#SBATCH --mail-type=BEGIN,END,FAIL
#! Specify your email address here otherwise you won't recieve emails!
#SBATCH --mail-user=sk875@cam.ac.uk

#SBATCH -p gpunodes
#! Specify a GRES (generic resource) of type gpu and how many you want
#SBATCH --gres gpu:1

#SBATCH --output=logs/annotate_extract-%j.out


SCRATCH='/scratchb/stlab-icgc/users/killco01'
SCRIPT='be_imagepath/tile_ann_extract.py'

usage() {                                 # Function: Print a help message.
  echo "Usage: $0 [ -p pml dir REQ ]  [ -a annotation dir REQ ] [ -o output dir REQ ] [ -r run name REQ ] [ -l annotations (IM,SQBS) REQ ]
	OPTIONAL: [ -e extract all PML or just those with XML annotations (def True) ] [ -m max_tiles (def 200) ] [ -n negative annotation (def Neg) ] [ -d overlap (0-1.0) (def 0.7) ] [ -t tlt (0-1 or False) ] [ -f fgt (0-100 or False) ]" 1>&2   
}
exit_abnormal() {                         # Function: Exit with error.
  usage
  exit 1
}

while getopts "p:o:a:r:d:n:l:m:e:t:f:" options; do
	echo "${options} ${OPTARG}"
        case "${options}" in
                p)
                        pml=${OPTARG}
                        ;;
                o)
                        outdir=${OPTARG}
                        ;;
		a)
			annotations=${OPTARG}
			;;
		r)
			runname="--run_name ${OPTARG}"
			;;			
		d) 
			overlap="--overlap ${OPTARG}"
			;;	
		n)
			negative="--negative ${OPTARG}"
			;;	

		l)
                        labels=$(echo $OPTARG | tr "," "\n")
                        labels="--annotations ${labels}"
                        ;;
		m)
			maxtiles="--max_tiles ${OPTARG}"
			;;
		e)
			extractall="--extract_all ${OPTARG}"
			;;
		t)
			tlt="--tlt ${OPTARG}"
			;;
		f)
			fgt="--fgt ${OPTARG}"
			;;
        esac
done

if [ "$pml" == "" ] || [ "$outdir" == "" ] || [ "$runname" == "" ] || [ "$labels" == "" ]; then
        exit_abnormal
fi

date
singularity exec \
  --nv \
  --bind $SCRATCH/singularity_recipe:/mnt slide_proc.simg \
  python3 /mnt/$SCRIPT \
  --pml_dir /mnt/$pml \
  --ann_dir /mnt/$annotations \
  --output_dir /mnt/$outdir $overlap $negative $runname $extractall $maxtiles $labels $fgt $tlt
date
