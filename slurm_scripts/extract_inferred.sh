#!/bin/bash

#! Give your job a name
#SBATCH -J extract_inferred_tiles
#! How many cores per task?
#SBATCH --cpus-per-task=4
#! How much memory do you need?
#SBATCH --mem=20G
#! How much wallclock time will be required?
#SBATCH --time=48:00:00
#! What types of email messages do you wish to receive?
#SBATCH --mail-type=BEGIN,END,FAIL
#! Specify your email address here otherwise you won't recieve emails!
#SBATCH --mail-user=sk875@cam.ac.uk

#SBATCH -p gpunodes
#! Specify a GRES (generic resource) of type gpu and how many you want
#SBATCH --gres gpu:1

#SBATCH --output=logs/extract_inferred-%j.out


SCRATCH='/scratchb/stlab-icgc/users/killco01'
SCRIPT='be_imagepath/extract_tiles.py'

usage() {                                 # Function: Print a help message.
  echo "Usage: $0 [ -p pml dir REQ ]  [ -s slide label file REQ ] [ -o output dir REQ ] [ -r run name REQ ] [ -l label column in file (Pathology, Dysplasia, etc) REQ ] 
	OPTIONAL: [ -t probability threshold (def 0.95) ] [ -m max_tiles (def 200) ] [ -f fgt (def 85) ]" 1>&2   
}
exit_abnormal() {                         # Function: Exit with error.
  usage
  exit 1
}

while getopts "p:o:r:l:s:m:f:t:" options; do
	echo "${options} ${OPTARG}"
        case "${options}" in
                p)
                        pml=${OPTARG}
                        ;;
                o)
                        outdir=${OPTARG}
                        ;;
		s)
			slidefile="--slide_list /mnt/${OPTARG}"
			;;
		r)
			runname="--run_name ${OPTARG}"
			;;			
		l)
                        label="--label ${OPTARG}"
                        ;;
		m)
			maxtiles="--max_tiles ${OPTARG}"
			;;
		f)
			fgt="--fgt ${OPTARG}"
			;;
		t)
			prob="--prob ${OPTARG}"
			;;
        esac
done

if [ "$pml" == "" ] || [ "$outdir" == "" ] || [ "$runname" == "" ] || [ "$label" == "" ]; then
        exit_abnormal
fi

date
singularity exec \
  --nv \
  --bind $SCRATCH/singularity_recipe:/mnt slide_proc.simg \
  python3 /mnt/$SCRIPT \
  --pml_dir /mnt/$pml \
  --output_dir /mnt/$outdir $slidefile $runname $maxtiles $label $fgt $prob
date
