#!/bin/python

import sys
import time
from datetime import date

import tempfile

import copy
import random
import shutil

from tqdm import tqdm

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import torchvision

from torchvision import datasets, models, transforms

import numpy as np
from skimage.transform import resize
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, balanced_accuracy_score, f1_score, precision_score, recall_score, roc_auc_score, roc_curve


import matplotlib.pyplot as plt


import pyvips as pv

sys.path.append('/mnt/slidl')
from slidl.slide import Slide
#from slidl.analysis import Analysis
#from slidl.processor import Processor
#from slidl.models.tissuedetector import tissueDetector


print("PyTorch version:", torch.__version__)
print("pyvips version:", pv.__version__)

def visualize_batch(inp, mean, std, title=None):
    inp = inp.numpy().transpose((1, 2, 0))
    #mean = means_and_stds['channel_means']
    #std = means_and_stds['channel_stds']
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    #plt.pause(0.001)
    #return plt
    plt.savefig(title)


def train_classification_model(model, dataloaders, dataset_sizes, criterion, optimizer, scheduler, num_epochs=25, classes=[], earlystop=False):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    if earlystop and earlystop > 1 or earlystop <= 0:
        raise ValueError("Earlystop must be greater than 0 and less than 1")

    if torch.cuda.device_count() > 1:
        model = nn.DataParallel(model)
    model = model.to(device)

    since = time.time()
    learning_stats = {'train': [], 'val': []}
    
    best_model_state_dict = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    for epoch in range(num_epochs):
        epoch = epoch+1
        print('Epoch {}/{}'.format(epoch, num_epochs))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            epoch_ground_truth = []
            epoch_predictions = []

            running_loss = 0.0
            running_corrects = 0

            if len(classes) > 0:
                correct_pred = {classname: 0 for classname in classes}
                total_pred = {classname: 0 for classname in classes}

            # Iterate over data.
            for inputs, labels in tqdm(dataloaders[phase]):
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                epoch_ground_truth = epoch_ground_truth + labels.data.tolist()
                epoch_predictions = epoch_predictions + preds.tolist()

                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            if scheduler:
                if phase == 'train':
                    scheduler.step()
                
            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]
            epoch_weighted_acc = balanced_accuracy_score(epoch_ground_truth, epoch_predictions)
            epoch_weighted_rec = recall_score(epoch_ground_truth, epoch_predictions, average='weighted')
            epoch_weighted_prec = precision_score(epoch_ground_truth, epoch_predictions, average='weighted')
            epoch_weighted_f1 = f1_score(epoch_ground_truth, epoch_predictions, average='weighted')

            if len(classes) > 100:
                for label, prediction in zip(labels, preds):
                    print('Preds:', preds)
                    #print(classes[label])
                    total_pred[classes[label]] += 1
                    
                    if label == prediction:
                        correct_pred[classes[label]] += 1

            learning_stats[phase].append(
                {'epoch':epoch,
		         'loss': epoch_loss,
                 'accuracy': epoch_acc, 
                 'weighted_accuracy': epoch_weighted_acc,
                 'weighted_precision': epoch_weighted_prec,
                 'weighted_recall': epoch_weighted_rec,
                 'weighted_f1': epoch_weighted_f1
            })

            #print('{} Loss: {:.4f} Acc: {:.4f}'.format(phase, epoch_loss, epoch_acc))
            print('{} Loss: {:.4f} Acc: {:.4f} Weighted Acc: {:.4f} Weighted Pre: {:.4f} Weighted Rec: {:.4f} Weighted F1: {:.4f}'.format(phase, epoch_loss, epoch_acc, epoch_weighted_acc, epoch_weighted_prec, epoch_weighted_rec, epoch_weighted_f1))

            if len(classes) > 100:
                for classname, correct_count in correct_pred.items():
                    accuracy = 100 * float(correct_count) / total_pred[classname]
                    print("Accuracy for class {:5s} is: {:.1f} %".format(classname,accuracy))

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_state_dict = copy.deepcopy(model.state_dict())

            if phase == 'val' and earlystop and epoch > num_epochs * 0.3:
                print("Testing early stop, ", earlystop, " accuracy improvement")
                prev_acc = next(item["accuracy"] for item in learning_stats["val"] if item["epoch"] == (epoch - 5)).item()
                current_acc = next(item["accuracy"] for item in learning_stats["val"] if item["epoch"] == epoch).item()
                if not current_acc >= (prev_acc + prev_acc * earlystop):
                    print("Stopping at epoch ", epoch, " with current accuracy=", round(current_acc, 3))
                    break
        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    #model.load_state_dict(best_model_state_dict)
    #return model, learning_stats
    return best_model_state_dict, learning_stats

