#!/bin/bash

#! Give your job a name
#SBATCH -J train_test_network
#! How many cores per task?
#SBATCH --cpus-per-task=4
#! How much memory do you need?
#SBATCH --mem=20G
#! How much wallclock time will be required?
#SBATCH --time=1:00:00
#! What types of email messages do you wish to receive?
#SBATCH --mail-type=END,FAIL
#! Specify your email address here otherwise you won't recieve emails!
#SBATCH --mail-user=sk875@cam.ac.uk
#! Uncomment this to prevent the job from being requeued (e.g. if
#! interrupted by node failure or system downtime):
##SBATCH --no-requeue
#! Make sure to use the gpunodes partition
#SBATCH -p gpunodes
#! Specify a GRES (generic resource) of type gpu and how many you want
#SBATCH --gres gpu:1
#SBATCH --output=logs/train_path_slurm-%j.out


SCRATCH='/scratchb/stlab-icgc/users/killco01'
SCRIPT='train_path.py'
#SING='slide_proc_pytorch1.8.simg'
SING='slide_proc.simg'

args=("$@")
tLen=${#args[@]}


if [ $tLen -lt 2 ]; then
        echo "Usage: $0 <tiles dir> <output dir> <datasize 0.1-1> <epochs> <run name> <labels: path or prog>"
        exit -1
fi

tiles=$1
outdir=$2

datasize="--datasize 1.0"
if [ $tLen -gt 2 ]; then
	datasize="--datasize $3"
fi

epoch="--epochs 5"
if [ $tLen -gt 3 ]; then
        epoch="--epochs $4"
fi

run=''
if [ $tLen -gt 4 ]; then
	run="--run_name $5"
fi

labels='--labels path'
if [ $tLen -gt 5 ]; then
	labels="--labels $6"
fi

# --nv mounts Nvidia CUDA libraries
date
singularity exec \
  --nv \
  --bind ${SCRATCH}/singularity_recipe:/mnt ${SING} \
  python3 /mnt/${SCRIPT} \
  --tiles_dir /mnt/${tiles} \
  --slide_list /mnt/slide_info_28072021.csv \
  --output_dir /mnt/${outdir} ${epoch} ${run} ${datasize} ${labels}
date
