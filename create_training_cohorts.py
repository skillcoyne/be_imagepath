import os
import sys
import glob
import argparse
import traceback
import warnings
import time
import csv

import pandas as pd
import random
import pickle
import math

from sklearn.model_selection import train_test_split

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--slide_list', type=str, help='csv file listing slides and whole-slide annotations')
parser.add_argument('--labels', default='Dysplasia', required=True, help='Column from the slide list to provide labels')
parser.add_argument('--output_dir', type=str, required=True, help='Write file to directory')

args = parser.parse_args()
print(args)

seed = 366
random.seed(seed)

if not os.path.exists(args.slide_list):
	print("File not found: ", args.slide_list)
	quit()

wsi = pd.read_csv(args.slide_list)

labels = list(pd.unique(wsi.loc[:,args.labels]))
labels.sort()

cases = {}
for lb in labels:
    cases[lb] = list(wsi.loc[wsi[str]==lb,'Basename'])

# 70%, 15%, 15% splits
wsi['Cohort'] = ''
for label, slides in cases.items():
    train_slides, test_slides = train_test_split(slides, test_size=0.30, random_state=42)
    test_slides, val_slides = train_test_split(test_slides, test_size=0.5, random_state=42)

    for index, row in wsi.iterrows():
        if row['Basename'] in train_slides:
            wsi.at[index, 'Cohort'] = 'train'
        if row['Basename'] in test_slides:
            wsi.at[index, 'Cohort'] = 'test'
        if row['Basename'] in val_slides:
            wsi.at[index, 'Cohort'] = 'val'

wsi.to_csv(os.path.join(args.output_dir, 'training_cohorts.csv'))