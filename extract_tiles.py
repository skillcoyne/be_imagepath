#!/bin/python

import os
import sys
import glob
import argparse
import traceback
import warnings
import time
import csv

import torch
import numpy as np
from torchvision import datasets, models, transforms

#import matplotlib.pyplot as plt
from skimage.transform import resize
from sklearn.model_selection import train_test_split

import pandas as pd
import random
import pickle
import math

import pyvips as pv


sys.path.append('/mnt/slidl')
from slidl.slide import Slide
#from slidl.analysis import Analysis
#from slidl.processor import Processor
#from slidl.models.tissuedetector import tissueDetector

from slide_functions import extractInferredTiles


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--pml_dir', type=str, help='Inferred slide images directory')
parser.add_argument('--slide_list',type=str,help='csv file listing slides')
parser.add_argument('--output_dir', type=str, help='')
parser.add_argument('--run_name', type=str, help='')
parser.add_argument('--max_tiles', type=int, default=200, help='')
parser.add_argument('--tile_size', type=int, default=224, help='')
parser.add_argument('--fgt', type=int, default=85, help='')
parser.add_argument('--label', default='Pathology', type=str, required=True, help='')
parser.add_argument('--prob', default=0.95, type=float, required=False, help='Probability threshold for inferred tile extraction')

args = parser.parse_args()
print(args)

if not os.path.exists(args.pml_dir):
	print("PML directory ", args.pml_dir, " not found.")
	quit()

if not os.path.exists(args.slide_list):
	print("File not found: ", args.slide_list)
	quit()

max_tiles=args.max_tiles
run_name = args.run_name + "_" + str(max_tiles) + "_" + str(args.prob)
results_path = os.path.join(args.output_dir, run_name)
if not os.path.isdir(results_path):
	os.makedirs(results_path)

print(results_path)

level=0 # extract tiles at highest resolution
mult=1 # no scaling


libdict_file=os.path.join(results_path, run_name+'_lib_dict.pt')
print("Lib dictionary file: " + libdict_file)

if os.path.exists(libdict_file):
    print('Loading in-progress lib dictionary from '+libdict_file)
    lib_dict = torch.load(libdict_file)
else:
    print("Creating new lib dictionary")
    lib_dict = {"slides": [], "size": args.tile_size, "mult": mult, "level": level, "foregroundLevelThreshold": args.fgt}

warnings.simplefilter('ignore')

pml_paths = glob.glob(os.path.join(args.pml_dir, '*.pml'))
pml_paths.sort()
print('PML files to extract ', len(pml_paths))

slides = pd.read_csv(args.slide_list)

global_channel_sums = np.zeros(3)
global_channel_squared_sums = np.zeros(3)
global_tile_count = 0
tile_counts = {}
if os.path.exists(os.path.join(results_path, 'tile_counts.p')):
    tile_counts = pickle.load(open(os.path.join(results_path, 'tile_counts.p'), 'rb'))

def get_file(name, filelist):
    matching_files = [s for s in filelist if os.path.basename(name) in s]
    if len(matching_files) <= 0:
        return(False)
    else:
        return(matching_files[0])

for sample_name in list(slides['Basename']):
	pml_file = get_file(sample_name, pml_paths)
	if not pml_file:
		print(sample_name+'.pml', " does not yet exist, skipping")
		continue

	tileClass = slides.loc[slides['Basename'] == sample_name ][args.label].item()
	emptyTiles = list(pd.unique(slides[args.label]))
	emptyTiles.remove(tileClass)
	print(tileClass, " ", emptyTiles)

	if pml_file in lib_dict['slides']:
		print("Already extracted " + pml_file + " skipping.")
		#print(tile_counts)
		global_channel_sums = np.add(global_channel_sums, tile_counts[sample_name]['channel_data']['channel_sums'])
		global_channel_squared_sums = np.add(global_channel_squared_sums, tile_counts[sample_name]['channel_data']['channel_squared_sums'])
		global_tile_count = global_tile_count + tile_counts[sample_name]['channel_data']['num_tiles']
		continue
	print('\tExtracting ' + pml_file )
	try:
		slidl_slide = Slide(pml_file, level=level)

		## Extract tiles from predicted regions
		channel_data = extractInferredTiles(slidl_slide, outputDir=results_path, probabilityThreshold=args.prob, numTilesToExtractPerClass=args.max_tiles, slideClass=tileClass, otherClasses=emptyTiles, classesToExtract='IM', foregroundLevelThreshold=args.fgt)
		#print(channel_data)

		lib_dict['slides'].append(pml_file) 		
		torch.save(lib_dict, f=libdict_file)

		global_channel_sums = np.add(global_channel_sums, channel_data['channel_sums'])
		global_channel_squared_sums = np.add(global_channel_squared_sums, channel_data['channel_squared_sums'])
		global_tile_count = global_tile_count + channel_data['num_tiles']

		tile_counts[sample_name] = {'max_per_class': max_tiles, 'channel_data': channel_data, 'foregroundLevelThreshold': args.fgt}		
		#print(tile_counts)
		pickle.dump(tile_counts, open(os.path.join(results_path, 'tile_counts.p'), 'wb'))
	except Exception as e:
		print(e.__class__, " error occurred: ", e)
		traceback.print_tb(e.__traceback__)
		traceback.print_exc()
	sys.stdout.flush()


total_pixels_per_channel = global_tile_count * args.tile_size * args.tile_size
global_channel_means = np.divide(global_channel_sums, total_pixels_per_channel)
global_channel_squared_means = np.divide(global_channel_squared_sums, total_pixels_per_channel)
global_channel_variances = np.subtract(global_channel_squared_means, np.square(global_channel_means))
global_channel_stds = np.sqrt(global_channel_variances * (total_pixels_per_channel / (total_pixels_per_channel-1)))
means_and_stds = {'channel_means': global_channel_means.tolist(), 'channel_stds': global_channel_stds.tolist()}

print(means_and_stds)
pickle.dump(means_and_stds, open(os.path.join(results_path, 'channel_means_and_stds.p'), 'wb'))

torch.save(lib_dict, f=libdict_file)
pickle.dump(tile_counts, open(os.path.join(results_path, 'tile_counts.p'), 'wb'))
print('Slides annotated and extracted:',len(lib_dict['slides']))
print("*** Finished ***")
