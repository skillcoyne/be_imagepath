#!/bin/bash

#! This file represents a very simple header header that you can use as the
#! basis for your own jobs. Copy this file and amend it.

#!#############################################################
#!#### Modify the options in this section as appropriate ######
#!#############################################################

#! Give your job a name
#SBATCH -J k100,ta,TGCT-WGD
#! How many cores per task?
#SBATCH --cpus-per-task=4
#! How much memory do you need?
#SBATCH --mem=20G
#! How much wallclock time will be required?
#SBATCH --time=10000:00:00
#! What types of email messages do you wish to receive?
#SBATCH --mail-type=BEGIN,END,FAIL
#! Specify your email address here otherwise you won't recieve emails!
#SBATCH --mail-user=adam.berman@cruk.cam.ac.uk
#! Uncomment this to prevent the job from being requeued (e.g. if
#! interrupted by node failure or system downtime):
##SBATCH --no-requeue
#! Make sure to use the gpunodes partition
#SBATCH -p gpunodes
#! Specify a GRES (generic resource) of type gpu and how many you want
#SBATCH --gres gpu:1

application="singularity exec --nv --bind /mnt/scratcha/fmlab/berman01/wsl_on_wsis:/mnt wsl_on_wsis_sklearn.simg python /mnt/2_MIL_train.py" 
options=" --output /mnt --trainval_lib /mnt/tcga_testicular_tile_dictionaries_train-val_75-25_split_lib_cluster-paths.pt --nepochs 100 --workers 4 --batch_size 32 --biomarker wgd --save_state_dicts --k 100 --no_pin_memory --save_all_val_state_dicts"
CMD="$application $options"
#eval $CMD
echo $CMD
